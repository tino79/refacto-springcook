package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.models.business.RecipeIngredientModel;

import java.util.ArrayList;

public interface RecipeIngredientRepository {
    ArrayList<RecipeIngredientModel> findAllByRecipeEntity(Recipe recipe);
    boolean existsByRecipeEntityNameAndIngredientEntityName(String recipe_name, String ingredient_name);
    RecipeIngredientModel getByRecipeEntityAndIngredientEntity(Recipe recipe, Ingredient ingredient);
    RecipeIngredientModel createRecipeIngredientModel(RecipeIngredientModel recipeIngredientModel);
    Boolean usesAllIngredients(String recipe_name, String[] ingredients);
    Float getIngredientQuantity(Recipe recipe, Ingredient ingredient);
}
