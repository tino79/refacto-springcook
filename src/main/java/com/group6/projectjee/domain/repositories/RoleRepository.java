package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.enums.ERole;
import com.group6.projectjee.infrastructure.persistence.entities.Role;

public interface RoleRepository {
    Role findRoleByName(ERole name);

}
