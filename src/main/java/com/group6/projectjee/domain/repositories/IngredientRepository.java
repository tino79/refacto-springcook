package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.business.Ingredient;

public interface IngredientRepository {
    Boolean ingredientExistsByName(String name);
    Boolean ingredientExistsById(Long id);
    Ingredient findIngredientByName(String name);
    Iterable<Ingredient> findAllIngredients();
    Ingredient createIngredient(Ingredient ingredient);
    Ingredient findIngredientById(Long id);
}
