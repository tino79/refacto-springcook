package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;

import java.util.ArrayList;

public interface RecipeRepository {
    Boolean recipeExistsByName(String name);
    Boolean recipeExistsById(Long id);
    Boolean recipeHasIngredient(Recipe recipe, Ingredient ingredient);
    Recipe findRecipeByName(String name);
    Recipe createRecipe(Recipe recipe);
    Recipe findRecipeById(Long id);
    ArrayList<Recipe> findAllRecipes();
    ArrayList<Ingredient> getIngredientsFromRecipe(Recipe recipe);
    ArrayList<Recipe> findRecipesByIngredient(Ingredient ingredient);
}
