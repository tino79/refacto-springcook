package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.users.User;

import java.util.ArrayList;

public interface UserRepository {
    User findUserByUsername(String username);
    User findUserById(Long id);
    Boolean userExistsByUsername(String username);
    Boolean userExistsByEmail(String email);
    Boolean userExistsById(Long id);
    User createUser(User user);
    void deleteUser(User user);
    ArrayList<User> findAllUsers();
}
