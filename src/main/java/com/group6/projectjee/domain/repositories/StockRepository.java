package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;

public interface StockRepository {
    Stock findStockByUser(User user);
    Stock createStock(Stock stock);
}
