package com.group6.projectjee.domain.repositories;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;

import java.util.List;

public interface StockIngredientRepository {
    boolean stockIngredientExistsByStockEntity(Stock stock);
    StockIngredientModel getStockIngredientByStockEntity(Stock stock);
    boolean stockIngredientExistsByStockEntityAndIngredientEntity(Stock stock, Ingredient ingredient);
    StockIngredientModel getStockIngredientByIngredientEntityAndStockEntity(Ingredient ingredient, Stock stock);
    Float getStockIngredientQuantity(Ingredient ingredient, Stock stock);
    float getStockIngredientMinLimit(Ingredient ingredient, Stock stock);
    List<Ingredient> getAllIngredientEntityByStock(Stock stock);
    void updateStockIngredientQuantity(Ingredient ingredient, Stock stock, Float newQuantity);
    void updateStockIngredientLimit(Ingredient ingredient, Stock stock, Float newLimit);
    StockIngredientModel createStockIngredient(StockIngredientModel stockIngredientModel);
    void deleteStockIngredient(StockIngredientModel stockIngredientModel);
    List<StockIngredientModel> getAllStockIngredient();
}
