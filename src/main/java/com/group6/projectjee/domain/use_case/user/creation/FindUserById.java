package com.group6.projectjee.domain.use_case.user.creation;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.UserRepository;


public class FindUserById {

    private final UserRepository userRepository;

    public FindUserById(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Find user given an id
     * @param userId : Long user Id
     * @return User if user exists else null
     */
    public User execute(long userId){
        if(this.userRepository.userExistsById(userId))
            return this.userRepository.findUserById(userId);
        else return null;
    }
}
