package com.group6.projectjee.domain.use_case.ingredient;

import com.group6.projectjee.domain.repositories.IngredientRepository;

public class IngredientExistsByName {

    private final IngredientRepository ingredientRepository;

    public IngredientExistsByName(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Finds if an ingredient exists given an ingredient name in database
     * @param name : String searched ingredient
     * @return true if database contains given ingredient else false
     */
    public Boolean execute(String name) {
        return this.ingredientRepository.ingredientExistsByName(name);
    }
}
