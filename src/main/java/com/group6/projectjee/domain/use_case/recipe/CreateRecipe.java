package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeRepository;


public class CreateRecipe {

    private final RecipeRepository recipeRepository;

    public CreateRecipe(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public Recipe execute(Recipe recipe) throws Exception {
        if (this.recipeRepository.recipeExistsByName(recipe.getName())){
            throw new Exception("Recipe already exist");
        }
        return this.recipeRepository.createRecipe(recipe);
    }
}
