package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;

import java.util.ArrayList;


public class GetIngredientsUnderLimit {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final StockIngredientRepository stockIngredientRepository;

    public GetIngredientsUnderLimit(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Get all user's ingredients under choosen limit
     * @param userId : Long user Id
     * @return ArrayList that contains all ingredients' quantity that is under limit
     */
    public ArrayList <Ingredient> execute(Long userId){
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            ArrayList <Ingredient> res = new ArrayList<>();
            Stock stock = this.stockRepository.findStockByUser(user);
            if(stock == null){
                this.stockRepository.createStock(new Stock("", user));
            }
            for (Ingredient ingredient:this.stockIngredientRepository.getAllIngredientEntityByStock(stock)) {
                if(this.stockIngredientRepository.getStockIngredientMinLimit(ingredient, stock) >
                        this.stockIngredientRepository.getStockIngredientQuantity(ingredient, stock)){
                    res.add(ingredient);
                }
            }
            return res;
        } else {
            throw new UsernameNotFoundException("");
        }
    }
}
