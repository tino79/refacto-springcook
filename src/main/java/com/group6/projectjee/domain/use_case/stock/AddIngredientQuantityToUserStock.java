package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;

public class AddIngredientQuantityToUserStock {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final StockIngredientRepository stockIngredientRepository;
    private final IngredientRepository ingredientRepository;

    public AddIngredientQuantityToUserStock(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository, IngredientRepository ingredientRepository) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.stockIngredientRepository = stockIngredientRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Add quantity to a given ingredient
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient id
     * @param quantity : float quantity to add
     * @return updated ingredient's quantity
     * @throws UsernameNotFoundException
     */
    public float execute(long userId, Long ingredientId, float quantity) {
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Stock stock = this.stockRepository.findStockByUser(user);
            if(stock == null) {
                this.stockRepository.createStock(new Stock("", user));
            }
            Ingredient ingredient = this.ingredientRepository.findIngredientById(ingredientId);
            this.stockIngredientRepository.updateStockIngredientQuantity(ingredient, stock, quantity);
            return this.stockIngredientRepository.getStockIngredientQuantity(ingredient, stock);
        } else {
            throw new UserNotFoundException(userId);
        }
    }
}
