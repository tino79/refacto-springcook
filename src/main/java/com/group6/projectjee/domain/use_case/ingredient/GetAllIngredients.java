package com.group6.projectjee.domain.use_case.ingredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.repositories.IngredientRepository;

public class GetAllIngredients {

    private final IngredientRepository ingredientRepository;

    public GetAllIngredients(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Get all ingredients from database
     * @return Iterable that contains all ingredients from database
     */
    public Iterable<Ingredient> execute(){
        return this.ingredientRepository.findAllIngredients();
    }
}
