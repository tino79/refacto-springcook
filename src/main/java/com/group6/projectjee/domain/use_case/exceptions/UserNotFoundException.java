package com.group6.projectjee.domain.use_case.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long id) {
        super(String.format("User not Found with id %d", id));
    }
}
