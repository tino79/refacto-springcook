package com.group6.projectjee.domain.use_case.user.creation;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.UserRepository;


public class GetAllUser {


    private final UserRepository userRepository;

    public GetAllUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Get all user from database
     * @return Iterable that contains all users from database
     */
    public Iterable<User> execute()
    {
        return this.userRepository.findAllUsers();
    }

}
