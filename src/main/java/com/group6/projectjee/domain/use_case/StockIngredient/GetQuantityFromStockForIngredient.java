package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class GetQuantityFromStockForIngredient {

    private final StockIngredientRepository stockIngredientRepository;

    public GetQuantityFromStockForIngredient(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Get an ingredient quantity given an ingredient and a stock
     * @param ingredient : ingredient
     * @param stock : stock
     * @return Float ingredient's quantity in the stock
     */
    public float execute(Ingredient ingredient, Stock stock) {
        Float result = this.stockIngredientRepository.getStockIngredientQuantity(ingredient, stock);
        if(result == null) {
            return 0f;
        }
        return result;
    }
}
