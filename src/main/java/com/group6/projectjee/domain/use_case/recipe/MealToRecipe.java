package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.infrastructure.persistence.entities.Meal;

import java.util.HashMap;
import java.util.Map;


public class MealToRecipe {

    private final RecipeRepository recipeRepository;
    private final RecipeIngredientRepository recipeIngredientRepository;
    private final IngredientRepository ingredientRepository;

    public MealToRecipe(RecipeRepository recipeRepository, RecipeIngredientRepository recipeIngredientRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Parse a meal from external api into usable recipe
     * @param meal : Meal parsed
     * @return new recipe from parsed data
     */
    public Recipe execute(Meal meal) throws Exception {
        Recipe recipe = this.recipeRepository.createRecipe(new Recipe(meal.getStrMeal()));
        HashMap<String, String> mealIngredients = meal.getIngredientsMap();
        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            this.recipeIngredientRepository.createRecipeIngredientModel(new RecipeIngredientModel(recipe,
                    this.ingredientRepository.findIngredientByName(entry.getKey()),
                    this.quantity(entry.getValue())));
        }
        return recipe;
    }

    /**
     * Get the quantity from external meal
     * @param quantity : String parsed quantity
     * @return Float quantity
     */
    public Float quantity(String quantity) {
        if(quantity.matches("([1-9]/[2-9])|½")) {
            String[] calc = quantity.split("/");
            if(Float.parseFloat(calc[1]) == 0){
                return Float.parseFloat(calc[0]);
            }
            return Float.parseFloat(calc[0])/Float.parseFloat(calc[1]);
        }
        quantity = quantity.split(" ")[0];
        if(quantity.replaceAll("[^0-9]", "").equals("")) {
            return 1.0f;
        }

        return Float.parseFloat(quantity.replaceAll("[^0-9]", ""));
    }

}
