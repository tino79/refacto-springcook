package com.group6.projectjee.domain.use_case.ingredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import org.springframework.dao.DataIntegrityViolationException;


public class CreateIngredient {

    private final IngredientRepository ingredientRepository;

    public CreateIngredient(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Create a new ingredient into Database
     * @param ingredient : Ingredient ingredient we want to add
     * @return created ingredient
     * @throws DataIntegrityViolationException
     */
    public Ingredient execute(Ingredient ingredient) throws DataIntegrityViolationException{
        return this.ingredientRepository.createIngredient(ingredient);
    }
}
