package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class CreateStockIngredient {

    private final StockIngredientRepository stockIngredientRepository;

    public CreateStockIngredient(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Insert a new StockIngredientModel into database
     * @param stockIngredient : StockIngredientModel to insert
     * @return created StockIngredientModel
     */
    public StockIngredientModel execute(StockIngredientModel stockIngredient) {
        return this.stockIngredientRepository.createStockIngredient(stockIngredient);
    }
}
