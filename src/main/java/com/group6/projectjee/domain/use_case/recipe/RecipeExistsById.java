package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;


public class RecipeExistsById {

    private final RecipeRepository recipeRepository;

    public RecipeExistsById(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Find recipe into database by given od
     * @param id : Long ingredient id
     * @return true if ingredient exists else
     * @throws RecipeNotFoundException
     */
    public Boolean execute(Long id) {
        if( this.recipeRepository.recipeExistsById(id)){
            return true;
        } else {
            throw new RecipeNotFoundException(id.toString());
        }
    }
}
