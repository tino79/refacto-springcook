package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeRepository;


public class GetAllRecipes {

    private final RecipeRepository recipeRepository;

    public GetAllRecipes(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Get all recipe from database
     * @return Iterable that contains all data from database
     */
    public Iterable<Recipe> execute(){
        return this.recipeRepository.findAllRecipes();
    }
}
