package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;


public class GetUserIngredientQuantity {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;

    public GetUserIngredientQuantity(UserRepository userRepository, StockRepository stockRepository) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
    }

    /**
     * Internal use only for calculating purposes
     * @param userId : Long user Id
     * @param ingredient : Long ingredient Id
     * @return Float quantity if user
     */
    public float execute(long userId, Ingredient ingredient){
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Stock stock = stockRepository.findStockByUser(user);
            try {
                return getIngredientQuantityFromStockEntity(stock, ingredient.getName());
            } catch (IngredientNotFoundException e){
                return -2;
            }
        } else {
            return -1;
        }
    }

    public float getIngredientQuantityFromStockEntity(Stock stock, String ingredient_name) {
        for (StockIngredient stockIngredient :
                stock.getStockIngredients()) {
            if (stockIngredient.getIngredientEntity().getName() == ingredient_name) {
                return stockIngredient.getQuantity();
            }
        }
        return 0.f;
    }
}
