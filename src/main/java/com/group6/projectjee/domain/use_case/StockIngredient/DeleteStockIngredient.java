package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class DeleteStockIngredient {

    private final StockIngredientRepository stockIngredientRepository;

    public DeleteStockIngredient(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    public void execute(StockIngredientModel stockIngredient) {
        this.stockIngredientRepository.deleteStockIngredient(stockIngredient);
    }
}
