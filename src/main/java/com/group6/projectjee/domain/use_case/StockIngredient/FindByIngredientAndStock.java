package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class FindByIngredientAndStock {

    private final StockIngredientRepository stockIngredientRepository;

    public FindByIngredientAndStock(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    public StockIngredientModel findByIngredientAndStock(Ingredient ingredient, Stock stock) {
        return this.stockIngredientRepository.getStockIngredientByIngredientEntityAndStockEntity(ingredient, stock);
    }
}
