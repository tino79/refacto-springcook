package com.group6.projectjee.domain.use_case.RecipeIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;


public class GetQuantityFromRecipeIngredient {

    private final RecipeIngredientRepository recipeIngredientRepository;

    public GetQuantityFromRecipeIngredient(RecipeIngredientRepository recipeIngredientRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    /**
     * Get ingredient's quantity in a given recipe
     * @param recipe : parsed recipe
     * @param ingredient : parsed ingredient
     * @return Float quantity if ingredient and recipe are found
     */
    public float execute(Recipe recipe, Ingredient ingredient){
        return this.recipeIngredientRepository.getByRecipeEntityAndIngredientEntity(recipe, ingredient).getQuantity();
    }
}
