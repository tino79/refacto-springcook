package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class GetAllIngredientsFromUser {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final StockIngredientRepository stockIngredientRepository;

    public GetAllIngredientsFromUser(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepositoryient) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.stockIngredientRepository = stockIngredientRepositoryient;
    }

    /**
     * Return all ingredients that given user has
     * @param userId : Long
     * @return ArrayList that contains all ingredients from user
     */
    public ArrayList <Ingredient> execute(long userId){
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Stock stock = this.stockRepository.findStockByUser(user);
            if(stock == null){
                this.stockRepository.createStock(new Stock("", user));
            }
            List<StockIngredientModel> stockIngredientModelList =this.stockIngredientRepository.getAllStockIngredient();
            return (ArrayList<Ingredient>)stockIngredientModelList
                    .stream()
                    .map(stockIngredientModel -> stockIngredientModel.getIngredient()).collect(Collectors.toList());
        } else {
            throw new UserNotFoundException(userId);
        }
    }
}
