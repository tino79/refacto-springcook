package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;

public class FindRecipeByName {

    private final RecipeRepository recipeRepository;

    public FindRecipeByName(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Find recipe into database by given name
     * @param name : String ingredient name
     * @return Recipe if ingredient exists else
     * @throws RecipeNotFoundException
     */
    public Recipe execute(String name){
        if(this.recipeRepository.recipeExistsByName(name))
            return this.recipeRepository.findRecipeByName(name);
        else {
            throw new RecipeNotFoundException(name);
        }
    }
}
