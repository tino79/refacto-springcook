package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;


public class FindRecipeById {

    private final RecipeRepository recipeRepository;

    public FindRecipeById(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Find recipe into database by given od
     * @param id : Long ingredient id
     * @return Recipe if ingredient exists else
     * @throws RecipeNotFoundException
     */
    public Recipe execute(Long id) {
        if (this.recipeRepository.recipeExistsById(id)){
            return this.recipeRepository.findRecipeById(id);
        } else {
            throw new RecipeNotFoundException(id.toString());
        }
    }
}
