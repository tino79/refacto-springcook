package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;

public class UpdateLimitForStockForIngredient {

    private final StockIngredientRepository stockIngredientRepository;

    public UpdateLimitForStockForIngredient(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Update an ingredient's limit in a given stock
     * @param ingredient : ingredient
     * @param stock : stock
     * @param quantity : Float new ingredient's limit
     */
    public void execute(Ingredient ingredient, Stock stock, float quantity) {
        this.stockIngredientRepository.updateStockIngredientLimit(ingredient, stock, quantity);
    }

}
