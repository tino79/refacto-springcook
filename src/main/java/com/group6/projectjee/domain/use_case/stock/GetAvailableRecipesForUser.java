package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;
import org.springframework.aop.AopInvocationException;

import java.util.ArrayList;


public class GetAvailableRecipesForUser {

    private final UserRepository userRepository;
    private final StockIngredientRepository stockIngredientRepository;
    private final RecipeRepository recipeRepository;

    public GetAvailableRecipesForUser(UserRepository userRepository, StockIngredientRepository stockIngredientRepository, RecipeRepository recipeRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
        this.userRepository = userRepository;
        this.recipeRepository = recipeRepository;
    }

    /**
     * Get all available recipes for a given user
     * @param userId : Long user id
     * @return ArrayList that contains all available recipes for user
     * @throws UsernameNotFoundException
     */
    public ArrayList<Recipe> execute(Long userId) {
        ArrayList<Recipe> res = new ArrayList<>();
        User user = this.userRepository.findUserById(userId);
        if(user != null) {
            Iterable<Recipe> allRecipes = this.recipeRepository.findAllRecipes();
            for(Recipe recipeEntity : allRecipes){
                try {
                    if (this.availableRecipeInternal(userId, recipeEntity)) res.add(recipeEntity);
                } catch (AopInvocationException aop){
                    //do Nothing SKIP recipe
                }
            }
            return res;
        } else {
            throw new UserNotFoundException(userId);
        }
    }


    public boolean availableRecipeInternal(Long userId, Recipe recipeEntity){
        ArrayList <Ingredient> ingredientEntities = new ArrayList<>();
        if(recipeEntity.getRecipeIngredients() == null) {
            return false;
        }
        for (RecipeIngredientModel recipeIngredient :
                recipeEntity.getRecipeIngredients()) {
            ingredientEntities.add(recipeIngredient.getIngredient());
        }
        for(Ingredient ingredient : ingredientEntities){
            float neededQuantity = getIngredientQuantityFromRecipeEntity(recipeEntity, ingredient.getName());
            float availableQuantity = this.stockIngredientRepository.getStockIngredientQuantity( ingredient,
                    this.userRepository.findUserById(userId).getStocks().get(0));
            if(availableQuantity < 0) return false;
            if(neededQuantity > availableQuantity) return false;
        }
        return true;
    }


    public float getIngredientQuantityFromRecipeEntity(Recipe recipe, String ingredient_name) {
        for (RecipeIngredientModel recipeIngredientModel :
                recipe.getRecipeIngredients()) {
            if (recipeIngredientModel.getIngredient().getName().equals(ingredient_name)) {
                return recipeIngredientModel.getQuantity();
            }
        }
        return 0.f;
    }
}
