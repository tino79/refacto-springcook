package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.infrastructure.persistence.entities.Meal;

import java.util.HashMap;
import java.util.Map;

public class CheckRecipeIngredients {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;

    public CheckRecipeIngredients(RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Add ingredients to database for each ingredients contained in external recipe
     * @param meal : Meal being parsed
     */
    public void execute(Meal meal) {
        HashMap<String, String> mealIngredients = meal.getIngredientsMap();

        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            if(!this.ingredientRepository.ingredientExistsByName(entry.getKey())) {
                BaseUnit unit = this.unit(entry.getValue());
                this.ingredientRepository.createIngredient(new Ingredient(entry.getKey(), unit));
            }
        }
    }

    /**
     * Transform a measure from external recipe into usable Enum BaseUnit
     * @param measure : String parsed
     * @return BaseUnit for given ingredient
     */
    public BaseUnit unit(String measure) {
        if(measure.matches("[0-9]+ ?g(rammes?)?")) {
            return BaseUnit.G;
        } else if(measure.matches("[0-9]+ ?(cl|centilitres?)")) {
            return BaseUnit.CL;
        } else if(measure.matches("[0-9]+ ?(kg|kilogrammes?)")) {
            return BaseUnit.KG;
        } else if(measure.matches("[0-9]+ ?(ml|millilitres?)")) {
            return BaseUnit.ML;
        } else if(measure.matches("[0-9]+ ?[lL](itres?)?")) {
            return BaseUnit.L;
        } else if(measure.matches("([uU]ne? )?([cC]oupe|[Tt]asse|[Gg]obelet)")) {
            return BaseUnit.CUP;
        } else if(measure.matches("(([1-9](/[1-9])? ?)|[Uu]ne )?[cC]uillères? à café.*")) {
            return BaseUnit.TEASPOON;
        } else if(measure.matches("(([1-9](/[1-9])? ?)|[Uu]ne )?[cC]uillères? à soupe.*")) {
            return BaseUnit.TABLESPOON;
        }
        return BaseUnit.NO_UNIT;
    }
}
