package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;


public class DeleteIngredientFromStock {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final IngredientRepository ingredientRepository;
    private final StockIngredientRepository stockIngredientRepository;

    public DeleteIngredientFromStock(UserRepository userRepository, StockRepository stockRepository, IngredientRepository ingredientRepository, StockIngredientRepository stockIngredientRepository) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.ingredientRepository = ingredientRepository;
        this.stockIngredientRepository = stockIngredientRepository;
    }

    public void execute(Long userId, String ingredient_name) {
        User user = this.userRepository.findUserById(userId);

        if (!this.ingredientRepository.ingredientExistsByName(ingredient_name)) {
            throw new IngredientNotFoundException(ingredient_name);
        }

        if (user != null){
            Stock stock = this.stockRepository.findStockByUser(user);
            StockIngredientModel stockIngredient = this.stockIngredientRepository.getStockIngredientByIngredientEntityAndStockEntity(
                    this.ingredientRepository.findIngredientByName(ingredient_name),
                    stock);
            if(stockIngredient != null) {
                this.stockIngredientRepository.deleteStockIngredient(stockIngredient);
            } else {
                throw new RuntimeException(ingredient_name + " isn't in " + user.getUsername() + "'s stock");
            }
        } else {
            throw new UsernameNotFoundException("");
        }
    }
}
