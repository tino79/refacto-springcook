package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class ExistByIngredientAndStock {

    private final StockIngredientRepository stockIngredientRepository;

    public ExistByIngredientAndStock(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    public Boolean execute(Stock stock, Ingredient ingredient) {
        return this.stockIngredientRepository.stockIngredientExistsByStockEntityAndIngredientEntity(stock, ingredient);
    }
}
