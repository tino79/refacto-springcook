package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;

import java.util.ArrayList;


public class GetRecipeIngredient {

    private final RecipeRepository recipeRepository;

    public GetRecipeIngredient(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Get all ingredients in a given recipe
     * @param recipe_id : parsed Recipe
     * @return ArrayList of all ingredients
     */
    public ArrayList<Ingredient> execute(Long recipe_id){
        if(!this.recipeRepository.recipeExistsById(recipe_id)) {
            throw new RecipeNotFoundException(recipe_id.toString());
        }
        return this.recipeRepository.getIngredientsFromRecipe(this.recipeRepository.findRecipeById(recipe_id));
    }
}
