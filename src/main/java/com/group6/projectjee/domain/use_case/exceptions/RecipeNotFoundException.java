package com.group6.projectjee.domain.use_case.exceptions;

public class RecipeNotFoundException  extends RuntimeException{
    public RecipeNotFoundException(String name) {
        super(String.format("recipe Not Found with name %s", name));
    }

    public RecipeNotFoundException(Long id) {
        super(String.format("recipe Not Found with name %d", id));
    }
}
