package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import org.springframework.aop.AopInvocationException;


public class GetLimitFromStockForIngredient {

    private final StockIngredientRepository stockIngredientRepository;

    public GetLimitFromStockForIngredient(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Get limit for a given ingredient in a given stock
     * @param ingredient : ingredient
     * @param stock : stock
     * @return Float ingredient's limit in stock
     * @throws AopInvocationException
     */
    public float execute(Ingredient ingredient, Stock stock) throws AopInvocationException{
        return this.stockIngredientRepository.getStockIngredientMinLimit(ingredient, stock);
    }
}
