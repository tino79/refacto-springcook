package com.group6.projectjee.domain.use_case.RecipeIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;


import java.util.ArrayList;


public class GetRecipeIngredientByRecipe {

    private final RecipeIngredientRepository recipeIngredientRepository;

    public GetRecipeIngredientByRecipe(RecipeIngredientRepository recipeIngredientRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    /**
     * Get all ingredients from a given recipe
     * @param recipe : Recipe
     * @return ArrayList of ingredients
     */
    public ArrayList<Ingredient> execute(Recipe recipe) {
        ArrayList<RecipeIngredientModel> results = this.recipeIngredientRepository.findAllByRecipeEntity(recipe);
        ArrayList<Ingredient> ingredient = new ArrayList<Ingredient>();
        for (RecipeIngredientModel entry : results) {
            ingredient.add(entry.getIngredient());
        }
        return ingredient;
    }
}
