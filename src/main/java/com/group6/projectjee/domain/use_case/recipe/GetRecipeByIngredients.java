package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.web.controllers.post.IngredientList;
import com.group6.projectjee.web.controllers.post.RecipeDto;

import java.util.ArrayList;


public class GetRecipeByIngredients {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;
    private final RecipeIngredientRepository recipeIngredientRepository;

    public GetRecipeByIngredients(RecipeRepository recipeRepository, IngredientRepository ingredientRepository, RecipeIngredientRepository recipeIngredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    public ArrayList<RecipeDto> execute(IngredientList ingredientList) {
        ArrayList<RecipeDto> result = new ArrayList<>();
        for (String ingredient :
                ingredientList.getIngredients()) {
            if(!this.ingredientRepository.ingredientExistsByName(ingredient)) {
                throw new IngredientNotFoundException(ingredient);
            }
            ArrayList<Recipe> recipesUsingIngredient = this.recipeRepository.findRecipesByIngredient(
                    this.ingredientRepository.findIngredientByName(ingredient)
            );
            for (Recipe recipe : recipesUsingIngredient) {
                if (!result.contains(recipe.toDto()) &&
                        this.recipeIngredientRepository.usesAllIngredients(recipe.getName(), ingredientList.getIngredients())) {
                    result.add(recipe.toDto());
                }
            }
        }
        return result;
    }
}
