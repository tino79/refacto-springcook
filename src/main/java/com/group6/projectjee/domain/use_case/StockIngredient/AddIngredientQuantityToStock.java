package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class AddIngredientQuantityToStock {

    private final StockIngredientRepository stockIngredientRepository;

    public AddIngredientQuantityToStock(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }


    /**
     * Add a given quantity to a given ingredient
     * @param ingredient : Long ingredient id
     * @param quantity : Float quantity to add
     * @param stock : Stock worked on
     */
    public void execute(Ingredient ingredient, float quantity, Stock stock){
        if(!this.stockIngredientRepository.stockIngredientExistsByStockEntityAndIngredientEntity(stock, ingredient)) {
            this.stockIngredientRepository.createStockIngredient(new StockIngredientModel());
        }
        this.stockIngredientRepository.updateStockIngredientQuantity(ingredient, stock, quantity);
    }
}
