package com.group6.projectjee.domain.use_case.RecipeIngredient;

import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;


public class RecipeIngredientExistsByName {

    private final RecipeIngredientRepository recipeIngredientRepository;

    public RecipeIngredientExistsByName(RecipeIngredientRepository recipeIngredientRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
    }


    /**
     * Find if a jointure exists between an ingredient and a recipe
     * @param recipe_name : String recipe name
     * @param ingredient_name : String ingredient name
     * @return True if both ingredient and recipe are linked, else false
     */
    public boolean execute(String recipe_name, String ingredient_name) {
        return this.recipeIngredientRepository.existsByRecipeEntityNameAndIngredientEntityName(recipe_name, ingredient_name);
    }
}
