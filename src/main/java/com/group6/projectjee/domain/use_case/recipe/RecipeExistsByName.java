package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.repositories.RecipeRepository;

public class RecipeExistsByName {

    private final RecipeRepository recipeRepository;

    public RecipeExistsByName(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public Boolean execute(String name) {
        return this.recipeRepository.recipeExistsByName(name);
    }
}
