package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;

import java.util.ArrayList;
import java.util.List;


public class GetAllIngredientsFromStock {


    private final StockIngredientRepository stockIngredientRepository;

    public GetAllIngredientsFromStock(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Get all Ingredients from a given stock
     * @param stock : stock
     * @return ArrayList that contains all ingredients in the stock
     */
    public ArrayList<Ingredient> execute(Stock stock){
        List <Ingredient> id = this.stockIngredientRepository.getAllIngredientEntityByStock(stock);
        ArrayList <Ingredient> res = new ArrayList<>();
        for (Ingredient ingredient: id){
            res.add(ingredient);
        }
        return res;
    }
}
