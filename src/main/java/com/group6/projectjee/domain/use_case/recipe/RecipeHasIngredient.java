package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;


public class RecipeHasIngredient {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;

    public RecipeHasIngredient(RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Find if a recipe contains given ingredient
     * @param recipe_name : String recipe name
     * @param ingredient_name : String ingredient name
     * @return True if recipe contains ingredients, else false
     * @throws RecipeNotFoundException if recipe is not found
     * @throws IngredientNotFoundException if ingredient is not found
     */
    public boolean execute(String recipe_name, String ingredient_name) {
        if(!this.recipeRepository.recipeExistsByName(recipe_name)) {
            throw new RecipeNotFoundException(recipe_name);
        }
        if(!this.ingredientRepository.ingredientExistsByName(ingredient_name)) {
            throw new IngredientNotFoundException(ingredient_name);
        }
        return this.recipeRepository.recipeHasIngredient(
                this.recipeRepository.findRecipeByName(recipe_name),
                this.ingredientRepository.findIngredientByName(ingredient_name)
        );
    }
}
