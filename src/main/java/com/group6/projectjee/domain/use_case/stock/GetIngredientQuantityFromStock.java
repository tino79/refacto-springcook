package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;


public class GetIngredientQuantityFromStock {

    private final UserRepository userRepository;
    private final IngredientRepository ingredientRepository;
    private final StockRepository stockRepository;
    private final StockIngredientRepository stockIngredientRepository;

    public GetIngredientQuantityFromStock(UserRepository userRepository, IngredientRepository ingredientRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository) {
        this.userRepository = userRepository;
        this.ingredientRepository = ingredientRepository;
        this.stockRepository = stockRepository;
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Get the quantity of a given ingredient from a given user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long Ingredient Id
     * @return Float ingredient's quantity
     * @throws UsernameNotFoundException if user not found
     */
    public float execute(long userId, Long ingredientId) {
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Stock stock = this.stockRepository.findStockByUser(user);
            if(stock == null){
                this.stockRepository.createStock(new Stock("", user));
            }
            Ingredient ingredient = this.ingredientRepository.findIngredientById(ingredientId);
            return this.stockIngredientRepository.getStockIngredientQuantity(ingredient, stock);
        } else {
            throw new UserNotFoundException(userId);
        }
    }
}
