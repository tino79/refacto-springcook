package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeRepository;

import java.util.ArrayList;
import java.util.Random;


public class GetRandomRecipe {

    private final RecipeRepository recipeRepository;

    public GetRandomRecipe(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Get a random recipe from database
     * @return Random recipe from database
     */
    public Recipe execute(){
        ArrayList<Recipe> allRecipe = this.recipeRepository.findAllRecipes();
        Random rand = new Random();
        return allRecipe.get(rand.nextInt(allRecipe.size()));
    }
}
