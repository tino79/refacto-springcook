package com.group6.projectjee.domain.use_case.recipe;

import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;
import com.group6.projectjee.web.controllers.post.RecipeIngredientDto;


public class AddIngredientToRecipe {

    private final RecipeIngredientRepository recipeIngredientRepository;
    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;

    public AddIngredientToRecipe(RecipeIngredientRepository recipeIngredientRepository, RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    public void execute(RecipeIngredientDto recipeIngredientDto) {
        if(recipeIngredientDto.getQuantity() <= 0) {
            throw new RuntimeException("Quantity must be greater than 0");
        }
        if(!this.recipeRepository.recipeExistsByName(recipeIngredientDto.getRecipe_name())) {
            throw new RecipeNotFoundException(recipeIngredientDto.getRecipe_name());
        }
        if(!this.recipeRepository.recipeExistsByName(recipeIngredientDto.getIngredient_name())) {
            throw new IngredientNotFoundException(recipeIngredientDto.getIngredient_name());
        }

        if(this.recipeIngredientRepository.existsByRecipeEntityNameAndIngredientEntityName(recipeIngredientDto.getRecipe_name(), recipeIngredientDto.getIngredient_name())) {
            throw new RuntimeException(recipeIngredientDto.getIngredient_name() + " is already used by " + recipeIngredientDto.getRecipe_name());
        }

        this.recipeIngredientRepository.createRecipeIngredientModel(new RecipeIngredientModel(
                this.recipeRepository.findRecipeByName(recipeIngredientDto.getRecipe_name()),
                this.ingredientRepository.findIngredientByName(recipeIngredientDto.getIngredient_name()),
                recipeIngredientDto.getQuantity()
        ));
    }
}
