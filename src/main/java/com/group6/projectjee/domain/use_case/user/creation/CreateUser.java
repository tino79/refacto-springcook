package com.group6.projectjee.domain.use_case.user.creation;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.UserRepository;


public class CreateUser {

    private final UserRepository userRepository;

    public CreateUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Insert a new user into database
     * @param user : user we're inserting into database
     * @return Inserted user
     */
    public User execute(User user){
        return this.userRepository.createUser(user);
    }

}
