package com.group6.projectjee.domain.use_case.RecipeIngredient;

import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;


public class CreateRecipeIngredient {
    private final RecipeIngredientRepository recipeIngredientRepository;

    public CreateRecipeIngredient(RecipeIngredientRepository recipeIngredientRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    public RecipeIngredientModel execute(RecipeIngredientModel recipeIngredientModel) {
        return this.recipeIngredientRepository.createRecipeIngredientModel(recipeIngredientModel);
    }
}
