package com.group6.projectjee.domain.use_case.exceptions;

import org.springframework.security.core.AuthenticationException;

public class UsernameNotFoundException  extends AuthenticationException {
    public UsernameNotFoundException(String username) {
        super(String.format ("User Not Found with username %s", username));
    }
}
