package com.group6.projectjee.domain.use_case.RecipeIngredient;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;


public class CheckRecipeUsesAllIngredients {

    private final RecipeIngredientRepository recipeIngredientRepository;

    public CheckRecipeUsesAllIngredients(RecipeIngredientRepository recipeIngredientRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    public boolean execute(Recipe recipe, String[] ingredients) {
        for (String ingredient : ingredients) {
            if (!this.recipeIngredientRepository.existsByRecipeEntityNameAndIngredientEntityName(recipe.getName(), ingredient)) {
                return false;
            }
        }
        return true;
    }
}
