package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;


public class GetIngredientLimitFromStock {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final StockIngredientRepository stockIngredientRepository;
    private final IngredientRepository ingredientRepository;

    public GetIngredientLimitFromStock(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository, IngredientRepository ingredientRepository) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.stockIngredientRepository = stockIngredientRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Get ingredient's limit
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @return Float ingredient's limit
     * @throws UsernameNotFoundException
     */
    public float execute(Long userId, Long ingredientId) {
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Stock stock = this.stockRepository.findStockByUser(user);
            if(stock == null){
                this.stockRepository.createStock(new Stock("", user));
            }
            Ingredient ingredient = this.ingredientRepository.findIngredientById(ingredientId);
            return this.stockIngredientRepository.getStockIngredientMinLimit(ingredient, stock);
        } else {
            throw new UserNotFoundException(userId);
        }
    }
}
