package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.stream.Collectors;


public class RecalculateStock {

    private final UserRepository userRepository;
    private final StockIngredientRepository stockIngredientRepository;
    private final RecipeIngredientRepository recipeIngredientRepository;
    private final RecipeRepository recipeRepository;

    public RecalculateStock(UserRepository userRepository, StockIngredientRepository stockIngredientRepository, RecipeIngredientRepository recipeIngredientRepository, RecipeRepository recipeRepository) {
        this.userRepository = userRepository;
        this.stockIngredientRepository = stockIngredientRepository;
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.recipeRepository = recipeRepository;
    }

    /**
     * Recalculate user's stock after cooking a recipe
     * @param userId : Long user id
     * @param recipeId : Long recipe id
     * @return ArrayList of UsedIngredients
     * @throws UsernameNotFoundException
     */
    public ArrayList <Ingredient> execute(Long userId, Long recipeId) {
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Recipe recipeEntity = this.recipeRepository.findRecipeById(recipeId);
            ArrayList <Ingredient> usedIngredientEntity = new ArrayList<>();
            ArrayList <Ingredient> ingredientEntities =
                    (ArrayList<Ingredient>) this.recipeIngredientRepository
                            .findAllByRecipeEntity(recipeEntity)
                            .stream()
                            .map(recipeIngredientModel -> recipeIngredientModel.getIngredient())
                            .collect(Collectors.toList());
            for (Ingredient ingredient: ingredientEntities) {
                usedIngredientEntity.add(ingredient);
                this.stockIngredientRepository.updateStockIngredientQuantity(ingredient,
                        user.getStocks().get(0),
                        this.stockIngredientRepository.getStockIngredientQuantity(ingredient, user.getStocks().get(0))
                                - this.recipeIngredientRepository.getIngredientQuantity(recipeEntity, ingredient));
            }
            return usedIngredientEntity;
        } else {
            throw new UserNotFoundException(userId);
        }
    }
}
