package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.repositories.StockRepository;


public class CreateStock {

    private final StockRepository stockRepository;

    public CreateStock(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }


    /**
     * Insert a new Stock into database
     * @param stock : Stock to insert
     * @return Inserted stock
     */
    public Stock execute(Stock stock) {
        return this.stockRepository.createStock(stock);
    }
}
