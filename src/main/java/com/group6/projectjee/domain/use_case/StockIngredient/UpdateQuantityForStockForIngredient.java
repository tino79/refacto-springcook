package com.group6.projectjee.domain.use_case.StockIngredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;


public class UpdateQuantityForStockForIngredient {

    private final StockIngredientRepository stockIngredientRepository;

    public UpdateQuantityForStockForIngredient(StockIngredientRepository stockIngredientRepository) {
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Update an ingredient's quantity in a given stock
     * @param ingredient : ingredient
     * @param stock : stock
     * @param quantity : Float new ingredient's quantity
     */
    public void execute(Ingredient ingredient, Stock stock, float quantity) {
        if(!this.stockIngredientRepository.stockIngredientExistsByStockEntityAndIngredientEntity(stock, ingredient)) {
            this.stockIngredientRepository.createStockIngredient(new StockIngredientModel());
        }
        this.stockIngredientRepository.updateStockIngredientQuantity(ingredient, stock, quantity);
    }

}
