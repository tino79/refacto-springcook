package com.group6.projectjee.domain.use_case.user.creation;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.UserRepository;


public class DeleteUser {


    private final UserRepository userRepository;

    public DeleteUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Delete a given user from database
     * @param user : user to delete
     */
    public void execute(User user){
        this.userRepository.deleteUser(user);
    }

}
