package com.group6.projectjee.domain.use_case.stock;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import com.group6.projectjee.domain.repositories.StockRepository;
import com.group6.projectjee.domain.repositories.UserRepository;
import com.group6.projectjee.domain.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;


public class UpdateIngredientLimitFromStock {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final IngredientRepository ingredientRepository;
    private final StockIngredientRepository stockIngredientRepository;

    public UpdateIngredientLimitFromStock(UserRepository userRepository, StockRepository stockRepository, IngredientRepository ingredientRepository, StockIngredientRepository stockIngredientRepository) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.ingredientRepository = ingredientRepository;
        this.stockIngredientRepository = stockIngredientRepository;
    }

    /**
     * Update limit to a given ingredient
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient id
     * @param quantity : float limit new limit
     * @return updated ingredient's quantity
     * @throws UsernameNotFoundException
     */
    public float execute(long userId, Long ingredientId, float quantity) {
        User user = this.userRepository.findUserById(userId);
        if(user != null){
            Stock stock = this.stockRepository.findStockByUser(user);
            if(stock == null){
                this.stockRepository.createStock(new Stock("", user));
            }
            Ingredient ingredient = this.ingredientRepository.findIngredientById(ingredientId);
            this.stockIngredientRepository.updateStockIngredientLimit(ingredient, stock, quantity);
            return this.stockIngredientRepository.getStockIngredientMinLimit(ingredient, stock);
        } else {
            throw new UserNotFoundException(userId);
        }
    }
}
