package com.group6.projectjee.domain.use_case.exceptions;

public class  IngredientNotFoundException extends RuntimeException {
    public IngredientNotFoundException(String name) {
        super(String.format("Ingredient Not Found with name %s", name));
    }


}
