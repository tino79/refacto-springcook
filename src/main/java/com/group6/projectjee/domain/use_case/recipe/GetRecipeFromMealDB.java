package com.group6.projectjee.domain.use_case.recipe;

import com.google.gson.Gson;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeMealDB;
import org.springframework.web.client.RestTemplate;


public class GetRecipeFromMealDB {

    private final RestTemplate restTemplate;
    private final Gson g;

    public GetRecipeFromMealDB(RestTemplate restTemplate, Gson g) {
        this.restTemplate = restTemplate;
        this.g = g;
    }

    /**
     * Get a meal from an external API
     * @param url : String url that contains external recipe
     * @return RecipeMealDB from url
     */
    public RecipeMealDB execute(String url) {
        return g.fromJson(this.restTemplate.getForObject(url, String.class), RecipeMealDB.class);
    }
}

