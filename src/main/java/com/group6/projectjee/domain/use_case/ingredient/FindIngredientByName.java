package com.group6.projectjee.domain.use_case.ingredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;


public class FindIngredientByName {

    private final IngredientRepository ingredientRepository;

    public FindIngredientByName(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Finds if an ingredient exists given an ingredient name in database
     * @param name : String searched ingredient
     * @return ingredient if ingredient exists else
     * @throws IngredientNotFoundException
     */
    public Ingredient execute(String name) {
        return this.ingredientRepository.findIngredientByName(name);
    }
}
