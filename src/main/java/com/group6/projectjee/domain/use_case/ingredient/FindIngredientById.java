package com.group6.projectjee.domain.use_case.ingredient;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;


public class FindIngredientById {

    private final IngredientRepository ingredientRepository;

    public FindIngredientById(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Finds if an ingredient exists given an ingredient id in database
     * @param id : Long searched ingredient
     * @return ingredient if ingredient exists else
     * @throws IngredientNotFoundException
     */
    public Ingredient execute(Long id){
        Ingredient ingredient= this.ingredientRepository.findIngredientById(id);
        if(ingredient != null) {
            return ingredient;
        } else {
            throw new IngredientNotFoundException("");
        }
    }
}
