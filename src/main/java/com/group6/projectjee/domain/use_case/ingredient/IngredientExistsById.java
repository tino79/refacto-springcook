package com.group6.projectjee.domain.use_case.ingredient;

import com.group6.projectjee.domain.repositories.IngredientRepository;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;


public class IngredientExistsById {

    private final IngredientRepository ingredientRepository;

    public IngredientExistsById(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Finds if an ingredient exists given an ingredient id in database
     * @param id : Long searched ingredient
     * @return true if database contains given ingredient else false
     */
    public Boolean execute(Long id) {
        if (this.ingredientRepository.ingredientExistsById(id)) {
            return this.ingredientRepository.ingredientExistsById(id);
        } else {
            throw new IngredientNotFoundException(id.toString());
        }
    }
}
