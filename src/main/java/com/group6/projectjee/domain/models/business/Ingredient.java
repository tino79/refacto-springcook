package com.group6.projectjee.domain.models.business;

import com.group6.projectjee.domain.models.ModelBase;
import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;

import java.util.List;

public class Ingredient extends ModelBase {

    private String name;
    private BaseUnit baseUnit;
    private List<RecipeIngredientModel> recipeIngredients;
    private List<StockIngredient> stockIngredients;

    public Ingredient(){
        super();
    }

    public Ingredient(String name, BaseUnit baseUnit, List<RecipeIngredientModel> recipeIngredients, List<StockIngredient> stockIngredients) {
        this.name = name;
        this.baseUnit = baseUnit;
        this.recipeIngredients = recipeIngredients;
        this.stockIngredients = stockIngredients;
    }

    public Ingredient(String key, BaseUnit unit) {
        this.name = key;
        this.baseUnit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecipeIngredientModel> getRecipeIngredients() {
        return recipeIngredients;
    }

    public BaseUnit getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(BaseUnit baseUnit) {
        this.baseUnit = baseUnit;
    }

    public List<StockIngredient> getStockIngredients() {
        return stockIngredients;
    }

    public void setStockIngredients(List<StockIngredient> stockIngredients) {
        this.stockIngredients = stockIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredientModel> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }
}
