package com.group6.projectjee.domain.models.business;

import com.group6.projectjee.domain.models.ModelBase;
import com.group6.projectjee.web.controllers.post.RecipeDto;

import java.util.Date;
import java.util.List;

public class Recipe extends ModelBase {

    private String name;

    private List<RecipeIngredientModel> recipeIngredients;
    private Date createdAt;
    private Date updatedAt;

    public Recipe(String name, List<RecipeIngredientModel> recipeIngredients, Date createdAt, Date updatedAt) {
        this.name = name;
        this.recipeIngredients = recipeIngredients;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public List<RecipeIngredientModel> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredientModel> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public Recipe(){
        super();
    }

    public Recipe(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public RecipeDto toDto() {
        return new RecipeDto(this.name);
    }

}
