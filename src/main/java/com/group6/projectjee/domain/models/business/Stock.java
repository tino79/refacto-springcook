package com.group6.projectjee.domain.models.business;

import com.group6.projectjee.domain.models.ModelBase;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;

import java.util.Date;
import java.util.List;

public class Stock extends ModelBase {

    private String name;
    private User user;
    private List<StockIngredient> stockIngredients;
    private Date createdAt;
    private Date updatedAt;

    public Stock(){
        super();
    }

    public Stock(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public Stock(String name, User user, List<StockIngredient> stockIngredients, Date createdAt, Date updatedAt) {
        this.name = name;
        this.user = user;
        this.stockIngredients = stockIngredients;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<StockIngredient> getStockIngredients() {
        return stockIngredients;
    }

    public void setStockIngredients(List<StockIngredient> stockIngredients) {
        this.stockIngredients = stockIngredients;
    }
}
