package com.group6.projectjee.domain.models.business;

import com.group6.projectjee.domain.models.ModelBase;

public class RecipeIngredientModel extends ModelBase {
    private Recipe recipe;
    private Ingredient ingredient;
    private float quantity;

    public RecipeIngredientModel(Recipe recipe, Ingredient ingredient, float quantity) {
        this.recipe = recipe;
        this.ingredient = ingredient;
        this.quantity = quantity;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
}
