package com.group6.projectjee.domain.models.business;

import com.group6.projectjee.domain.models.ModelBase;

public class StockIngredientModel extends ModelBase {
    private Stock stock;
    private Ingredient ingredient;
    private float quantity;
    private float minQuantity;

    public StockIngredientModel() {
    }

    public StockIngredientModel(Stock stock, Ingredient ingredient, float quantity, float minQuantity) {
        this.stock = stock;
        this.ingredient = ingredient;
        this.quantity = quantity;
        this.minQuantity = minQuantity;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public float getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(float minQuantity) {
        this.minQuantity = minQuantity;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
}
