package com.group6.projectjee.web.controllers.post;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.web.configs.validators.BaseUnitConstraint;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IngredientDto {
    @NotBlank(message = "name ne peut être vide")
    private String name;

    @BaseUnitConstraint
    private BaseUnit baseUnit;
}
