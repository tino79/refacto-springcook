package com.group6.projectjee.web.controllers;

import com.google.gson.Gson;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeMealDB;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.domain.use_case.recipe.GetRecipeFromMealDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/mealdb", produces = {MediaType.APPLICATION_JSON_VALUE, "text/plain;charset=UTF-8"})
public class MealDBController {

    private final GetRecipeFromMealDB getRecipeFromMealDB;
    private final Gson g;
    private final Logger logger;

    @Autowired
    public MealDBController(GetRecipeFromMealDB getRecipeFromMealDB) {
        this.getRecipeFromMealDB = getRecipeFromMealDB;
        this.g = new Gson();
        logger = Logger.getLogger("MealDBController");
    }

    /**
     * Get a random recipe from an external API
     * @return a string that contains data from a random recipe
     * @throws IngredientNotFoundException
     */
    @GetMapping
    public String getRecipe() throws IngredientNotFoundException, Exception {
        try {
            RecipeMealDB recipe = getRecipeFromMealDB.execute("https://www.themealdb.com/api/json/v1/1/random.php");
            recipe.meals.get(0).toFrench();
            //mealDBService.checkIngredients(recipe.meals.get(0));
            //mealDBService.toRecipe(recipe.meals.get(0));
            return g.toJson(recipe);
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
    }
}