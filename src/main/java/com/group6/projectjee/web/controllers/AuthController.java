package com.group6.projectjee.web.controllers;

import com.group6.projectjee.domain.models.enums.ERole;
import com.group6.projectjee.infrastructure.persistence.entities.Role;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.web.controllers.post.LoginRequest;
import com.group6.projectjee.web.controllers.post.SignupRequest;
import com.group6.projectjee.web.configs.response.JwtResponse;
import com.group6.projectjee.web.configs.response.MessageResponse;
import com.group6.projectjee.infrastructure.persistence.dal.CrudRoleRepository;
import com.group6.projectjee.infrastructure.persistence.dal.CrudUserRepository;
import com.group6.projectjee.web.jwt.JwtUtils;
import com.group6.projectjee.web.configs.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    CrudUserRepository crudUserRepository;

    @Autowired
    CrudRoleRepository crudRoleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    /**
     * Allows user to log-in if information are corrects else forbids authentication
     * @param loginRequest : LoginRequest that contains all Login information
     * @return Response Entity that said if Authentication is successful or why it failed
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest ){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(   jwt,
                                                    userDetails.getId(),
                                                    userDetails.getUsername(),
                                                    userDetails.getEmail(),
                                                    roles));
    }

    /**
     * Create a new account if conditions are met
     * @param signUpRequest
     * @return
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (crudUserRepository.userExistsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (crudUserRepository.userExistsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        UserEntity userEntity = new UserEntity(signUpRequest.getFirstname(),
                signUpRequest.getLastname(),
                signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = crudRoleRepository.findRoleByName(ERole.ROLE_USER);
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = crudRoleRepository.findRoleByName(ERole.ROLE_ADMIN);
                        roles.add(adminRole);

                        break;
                    default:
                        Role userRole = crudRoleRepository.findRoleByName(ERole.ROLE_USER);
                        roles.add(userRole);
                }
            });
        }

        userEntity.setRoles(roles);
        crudUserRepository.save(userEntity);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }


}
