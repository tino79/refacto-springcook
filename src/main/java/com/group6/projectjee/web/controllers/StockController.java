package com.group6.projectjee.web.controllers;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.use_case.StockIngredient.AddIngredientQuantityToStock;
import com.group6.projectjee.domain.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.RecipeNotFoundException;
import com.group6.projectjee.domain.use_case.exceptions.UsernameNotFoundException;
import com.group6.projectjee.domain.use_case.ingredient.FindIngredientById;
import com.group6.projectjee.domain.use_case.stock.*;
import com.group6.projectjee.domain.use_case.user.creation.CreateUser;
import com.group6.projectjee.web.controllers.post.StockDto;
import com.group6.projectjee.web.controllers.responses.ResponseFail;
import com.group6.projectjee.web.controllers.responses.ResponseSuccess;
import com.group6.projectjee.web.controllers.tomap.IngredientToMap;
import com.group6.projectjee.web.controllers.tomap.RecipeToMap;
import org.springframework.aop.AopInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/stock")
@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
public class StockController {

    private final FindIngredientById findIngredientById;
    private final GetAllIngredientsFromUser GetAllIngredientsFromUser;
    private final GetIngredientQuantityFromStock getIngredientQuantityFromStock;
    private final AddIngredientQuantityToStock addIngredientQuantityToStock;
    private final UpdateIngredientQuantityFromStock updateQuantity;
    private final GetIngredientLimitFromStock getIngredientLimitFromStock;
    private final UpdateIngredientLimitFromStock updateIngredientLimitFromStock;
    private final RecalculateStock recalculateStock;
    private final GetIngredientsUnderLimit getIngredientsUnderLimit;
    private final GetAvailableRecipesForUser getAvailableRecipesForUser;
    private final com.group6.projectjee.domain.use_case.user.creation.FindUserById findByUserId;
    private final CreateUser createUser;
    private final com.group6.projectjee.domain.use_case.stock.CreateStock createStock;
    private final DeleteIngredientFromStock deleteIngredientFromStock;
    private final AddIngredientQuantityToUserStock addIngredientQuantityToUserStock;

    @Autowired
    public StockController(FindIngredientById findIngredientById, GetAllIngredientsFromUser getAllIngredientsFromUser, GetIngredientQuantityFromStock getIngredientQuantityFromStock, AddIngredientQuantityToStock addIngredientQuantityToStock, UpdateIngredientQuantityFromStock updateQuantity, GetIngredientLimitFromStock getIngredientLimitFromStock, UpdateIngredientLimitFromStock updateIngredientLimitFromStock, RecalculateStock recalculateStock, GetIngredientsUnderLimit getIngredientsUnderLimit, GetAvailableRecipesForUser getAvailableRecipesForUser, com.group6.projectjee.domain.use_case.user.creation.FindUserById findByUserId, CreateUser createUser, com.group6.projectjee.domain.use_case.stock.CreateStock createStock, DeleteIngredientFromStock deleteIngredientFromStock, AddIngredientQuantityToUserStock addIngredientQuantityToUserStock){
        this.findIngredientById = findIngredientById;
        GetAllIngredientsFromUser = getAllIngredientsFromUser;
        this.getIngredientQuantityFromStock = getIngredientQuantityFromStock;
        this.addIngredientQuantityToStock = addIngredientQuantityToStock;
        this.updateQuantity = updateQuantity;
        this.getIngredientLimitFromStock = getIngredientLimitFromStock;
        this.updateIngredientLimitFromStock = updateIngredientLimitFromStock;
        this.recalculateStock = recalculateStock;
        this.getIngredientsUnderLimit = getIngredientsUnderLimit;
        this.getAvailableRecipesForUser = getAvailableRecipesForUser;
        this.findByUserId = findByUserId;
        this.createUser = createUser;
        this.createStock = createStock;
        this.deleteIngredientFromStock = deleteIngredientFromStock;
        this.addIngredientQuantityToUserStock = addIngredientQuantityToUserStock;
    }

    /**
     * Get all ingredients that user has
     * @param userId : Long user Id
     * @return success Response entity that contains all User's ingredients if user exists,
     * else Failed Response entity
     */
    @GetMapping("/{userId}/ingredient")
    public ResponseEntity<?> getUserIngredients(@PathVariable Long userId) {
        try{
            return new ResponseEntity<>(IngredientToMap.ingredientEntityListToMap(
                    this.GetAllIngredientsFromUser.execute(userId)), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get an ingredient Quantity from user's stock
     * @param userId : user Id
     * @param ingredientId : ingredient Id
     * @return success Response entity that contains Ingredient's quantity possessed by user if user and ingredient exist,
     * else, return failed response
     */
    @GetMapping("/{userId}/ingredient/{ingredientId}/quantity")
    public ResponseEntity<?> getUserIngredientQuantity(@PathVariable Long userId, @PathVariable Long ingredientId) {
        try{
            return new ResponseEntity<>(IngredientToMap.ingredientEntityQuantityToMap(
                        this.findIngredientById.execute(ingredientId),
                        this.getIngredientQuantityFromStock.execute(userId, ingredientId),
                        this.findIngredientById.execute(ingredientId).getBaseUnit()),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException ex){
            return new ResponseEntity<>(new ResponseFail("Ingredient does not exist"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Add Quantity to an ingredient in user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @param quantity : float Quantity added
     * @return Success response entity that contains the updated stock quantity for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PostMapping("/{userId}/ingredient/{ingredientId}/quantity/{quantity}")
    public ResponseEntity<?> addUserIngredientQuantity(@PathVariable Long userId, @PathVariable Long ingredientId, @PathVariable float quantity) {
        try{
            return new ResponseEntity<>(IngredientToMap.addedQuantityToMap(
                        this.findIngredientById.execute(ingredientId),
                        this.getIngredientQuantityFromStock.execute(userId, ingredientId),
                        this.addIngredientQuantityToUserStock.execute(userId, ingredientId, quantity)),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException ex){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update Ingredient quantity from user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @param quantity : float quantity to update
     * @return Success response entity that contains the updated stock quantity for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PatchMapping("/{userId}/ingredient/{ingredientId}/quantity/{quantity}")
    public ResponseEntity<?> updateIngredientQuantity(@PathVariable Long userId, @PathVariable Long ingredientId, @PathVariable Float quantity) {
        try{
            return new ResponseEntity<>(IngredientToMap.addedQuantityToMap(
                        this.findIngredientById.execute(ingredientId),
                        this.getIngredientQuantityFromStock.execute(userId, ingredientId),
                        this.updateQuantity.execute(userId, ingredientId, quantity)),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update Ingredient limit for an ingredient from user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @param quantity : float Quantity to add
     * @return Success response entity that contains the updated limit in stock for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PatchMapping("/{userId}/ingredient/{ingredientId}/limit-quantity/{quantity}")
    public ResponseEntity<?> updateIngredientLimit(@PathVariable Long userId, @PathVariable Long ingredientId, @PathVariable Float quantity) {
        try{
            return new ResponseEntity<>(IngredientToMap.updatedLimitToMap(
                    this.findIngredientById.execute(ingredientId),
                    this.getIngredientLimitFromStock.execute(userId, ingredientId),
                    this.updateIngredientLimitFromStock.execute(userId, ingredientId, quantity)
            ), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get Limit from ingredient from user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @return Success response entity that contains the limit for ingredient from user stock if user and ingredient exist
     * else return fail response entity
     */
    @GetMapping("/{userId}/ingredient/{ingredientId}/limit-quantity")
    public ResponseEntity<?> getIngredientLimit(@PathVariable Long userId, @PathVariable Long ingredientId) {
        try{
            return new ResponseEntity<>(IngredientToMap.getLimitToMap(
                    this.findIngredientById.execute(ingredientId),
                    this.getIngredientLimitFromStock.execute(userId, ingredientId)
            ), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }  catch (IngredientNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Recalculate stock quantity for user's stock after Ingredient was cooked
     * @param userId : Long user Id
     * @param recipeId : Long ingredient Id
     * @return Success response entity that contains the updated stock quantity for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PatchMapping("/{userId}/recipe/{recipeId}/recalculate-stock")
    public ResponseEntity<?> recalculateStock(@PathVariable Long userId, @PathVariable Long recipeId) {
        try{
            return new ResponseEntity<>(RecipeToMap.recalculateToMap(
                    this.recalculateStock.execute(userId, recipeId),
                    this.getIngredientsUnderLimit.execute(userId)),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }  catch (RecipeNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Recipe not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock lacks of ingredient"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all available recipes for an user
     * @param userId :Long user Id
     * @return ResponseEntity success that contains all available recipes for given user if user exists
     * else return fail response entity
     */
    @GetMapping("/{userId}/available-recipe")
    public ResponseEntity<?> getAvailableRecipes(@PathVariable Long userId) {
        try{
            return new ResponseEntity<>(RecipeToMap.availableRecipesToMap( this.getAvailableRecipesForUser.execute(userId)), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Create a new stock in database
     * @param stock : StockDto stock
     * @return A new stock Entity if user from stock exists else error
     */
    @PostMapping
    @ResponseStatus(CREATED)
    public Stock create(@RequestBody @Valid StockDto stock){
        User user = this.findByUserId.execute(stock.getUser());
        if(user != null){
            return createStock.execute(new Stock(stock.getName(), user));
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }

    /**
     * Delete an ingredient from a user's stock
     * @param userId User's id to remove the ingredient from
     * @param ingredient_name Name of the ingredient to remove
     * @return Response entity to confirm deletion from user's stock, else return a failure message
     */
    @DeleteMapping("/{userId}/ingredient/{ingredient_name}")
    public ResponseEntity<?> deleteIngredientFromStock(@PathVariable Long userId, @PathVariable String ingredient_name) {
        try {
            this.deleteIngredientFromStock.execute(userId, ingredient_name);
            return new ResponseEntity<>(new ResponseSuccess(ingredient_name + " has been removed from user " + userId, 0), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseFail(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}
