package com.group6.projectjee.web.controllers.post;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecipeDto {


    @NotBlank(message = "name ne peut être vide")
    private String name;

    private ArrayList<IngredientQuantityDto> ingredients;

    public RecipeDto(String name) {
        this.name = name;
    }
}
