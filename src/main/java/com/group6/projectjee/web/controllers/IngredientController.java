package com.group6.projectjee.web.controllers;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.use_case.ingredient.*;
import com.group6.projectjee.web.controllers.post.IngredientDto;
import com.group6.projectjee.web.controllers.responses.ResponseFail;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/ingredient")
public class  IngredientController {

    private final FindIngredientByName findIngredientByName;
    private final CreateIngredient createIngredient;
    private final GetAllIngredients getAllIngredients;

    @Autowired
    public IngredientController(FindIngredientByName findIngredientByName, CreateIngredient createIngredient, GetAllIngredients getAllIngredients, IngredientExistsByName ingredientExistsByName, IngredientExistsById ingredientExistsById) {
        this.findIngredientByName = findIngredientByName;
        this.createIngredient = createIngredient;
        this.getAllIngredients = getAllIngredients;
    }

    /**
     * Insert a new Ingredient into database
     * @param ingredient : IngredientDto that contains ingredient's data
     * @return New Ingredient Entity
     */
    @PostMapping
    public ResponseEntity<?> create(@RequestBody @Valid IngredientDto ingredient) {
        try {
            Ingredient newIngredient = this.createIngredient.execute(new Ingredient(ingredient.getName(), ingredient.getBaseUnit()));
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("success", true);
            map.put("added ingredient", newIngredient.getName());
            return new ResponseEntity<>(map, HttpStatus.CREATED);
        }catch (DataIntegrityViolationException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient already exists"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all ingredients from database
     * @return An iterable that contains all Ingredients from database
     */
    @GetMapping("")
    @NotFound
    public Iterable<Ingredient> getIngredients() { return this.getAllIngredients.execute();}

    /**
     * Get an ingredient from database that has given name
     * @param name : String ingredient name
     * @return Ingredient entity if ingredient exists
     * else return null
     */
    @GetMapping("/{name}")
    @NotFound
    public Ingredient getIngredientByName(@PathVariable String name) {
        return this.findIngredientByName.execute(name);
    }

}
