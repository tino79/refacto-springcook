package com.group6.projectjee.web.controllers.post;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecipeIngredientDto {
    @NotBlank(message = "recipe_name ne peut être vide")
    private String recipe_name;

    @NotBlank(message = "ingredient_name ne peut être vide")
    private String ingredient_name;

    @NotBlank(message = "quantity ne peut être null")
    @Min(value = 0, message = "quantity doit être supérieure à 0")
    private float quantity;
}