package com.group6.projectjee.web.controllers.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {
    @NotBlank(message = "username ne doit pas être vide")
    @Size(min = 3, max = 20)
    private String username;


    @NotBlank(message = "email ne doit pas être vide")
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank(message = "firstname ne doit pas être vide")
    @Size(min = 3, max = 30)
    private String firstname;

    @NotBlank(message = "lastname ne doit pas être vide")
    @Size(min = 3, max = 30)
    private String lastname;

    private Set<String> role;

    @NotBlank(message = "password ne doit pas être vide")
    @Size(min = 6, max = 40)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstname(String firstname) { this.firstname = firstname; }

    public void setLastname(String lastname) { this.lastname = lastname; }

    public String getFirstname() { return firstname; }

    public String getLastname() { return lastname; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRole() {
        return this.role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }
}
