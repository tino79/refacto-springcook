package com.group6.projectjee.web.controllers;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.domain.use_case.user.creation.GetAllUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private final GetAllUser getAllUser;

    @Autowired
    public UserController(GetAllUser getAllUser){
        this.getAllUser = getAllUser;
    }

    /**
     * Get all users from database
     * @return An iterable that contains all users
     */
    @GetMapping
    public Iterable<User> getUsers() { return this.getAllUser.execute();}
}
