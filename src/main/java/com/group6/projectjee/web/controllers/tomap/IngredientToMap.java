package com.group6.projectjee.web.controllers.tomap;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.enums.BaseUnit;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class IngredientToMap {

    public static Map<String, Object> ingredientEntityQuantityToMap(Ingredient ingredient, float quantity, BaseUnit unit){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredient.getName());
        map.put("quantity", quantity);
        map.put("unit", unit);
        return map;
    }

    public static Map<String, Object> ingredientEntityListToMap(ArrayList<Ingredient> ingredientEntityList){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredients", ingredientEntityList);
        return map;
    }

    public static Map<String, Object> addedQuantityToMap(Ingredient ingredient, float previousQuantity, float newQuantity){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredient.getName());
        map.put("previous_quantity", previousQuantity);
        map.put("new_quantity", newQuantity);
        return map;
    }

    public static Map<String, Object> updatedLimitToMap(Ingredient ingredient, float previousLimit, float newLimit){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredient.getName());
        map.put("previous_limit", previousLimit);
        map.put("new_limit", newLimit);
        return map;
    }

    public static Map<String, Object> getLimitToMap(Ingredient ingredient, float limit){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredient.getName());
        map.put("limit", limit);
        return map;
    }

}
