package com.group6.projectjee.web.configs;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.infrastructure.persistence.adapters.UserAdapter;
import com.group6.projectjee.infrastructure.persistence.dal.CrudUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    CrudUserRepository crudUserRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = crudUserRepository.findUserByUsername(username);
        return UserDetailsImpl.build(UserAdapter.parse(user));
    }
}
