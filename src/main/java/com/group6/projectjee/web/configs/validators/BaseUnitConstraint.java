package com.group6.projectjee.web.configs.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BaseUnitValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE} )
@Retention( RetentionPolicy.RUNTIME)
public @interface BaseUnitConstraint {
    String message() default "Unite de l'ingredient non valide";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
