package com.group6.projectjee.web.configs.validators;

import com.group6.projectjee.domain.models.enums.BaseUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BaseUnitValidator implements ConstraintValidator<BaseUnitConstraint, Object> {
    @Override
    public void initialize(BaseUnitConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) { return BaseUnit.contains(value.toString());}

}
