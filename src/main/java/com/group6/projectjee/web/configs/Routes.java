package com.group6.projectjee.web.configs;

import org.apache.commons.lang3.ArrayUtils;

public class Routes {
    public static final String[] swaggerRoutes = {
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

    public static final String[] publicRoutes = {
            "/api/test/**",
            "/actuator/**",
            "/error"
    };

    public static String[] getPublicRoutes() {
        return ArrayUtils.addAll(swaggerRoutes, publicRoutes);
    }


}
