package com.group6.projectjee.web.configs;

import com.group6.projectjee.domain.repositories.*;
import com.group6.projectjee.domain.use_case.RecipeIngredient.*;
import com.group6.projectjee.domain.use_case.StockIngredient.*;
import com.group6.projectjee.domain.use_case.ingredient.*;
import com.group6.projectjee.domain.use_case.recipe.*;
import com.group6.projectjee.domain.use_case.stock.*;
import com.group6.projectjee.domain.use_case.user.creation.CreateUser;
import com.group6.projectjee.domain.use_case.user.creation.DeleteUser;
import com.group6.projectjee.domain.use_case.user.creation.FindUserById;
import com.group6.projectjee.domain.use_case.user.creation.GetAllUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    //-----INGREDIENT-------//
    @Bean
    public CreateIngredient createIngredient(IngredientRepository ingredientRepository){
        return new CreateIngredient(ingredientRepository);
    }

    @Bean
    public FindIngredientById findIngredientById(IngredientRepository ingredientRepository){
        return new FindIngredientById(ingredientRepository);
    }

    @Bean
    public FindIngredientByName findIngredientByName(IngredientRepository ingredientRepository){
        return new FindIngredientByName(ingredientRepository);
    }

    @Bean
    public GetAllIngredients getAllIngredients(IngredientRepository ingredientRepository){
        return new GetAllIngredients(ingredientRepository);
    }

    @Bean
    public IngredientExistsByName ingredientExistsByName(IngredientRepository ingredientRepository){
        return new IngredientExistsByName(ingredientRepository);
    }

    @Bean
    public IngredientExistsById ingredientExistsById(IngredientRepository ingredientRepository){
        return new IngredientExistsById(ingredientRepository);
    }

    //-----------------------------------------------------//

    //-----------RECIPE----------------------------------//

    @Bean
    public AddIngredientToRecipe addIngredientToRecipe(RecipeIngredientRepository recipeIngredientRepository, RecipeRepository recipeRepository, IngredientRepository ingredientRepository){
        return new AddIngredientToRecipe(recipeIngredientRepository, recipeRepository, ingredientRepository);
    }

    @Bean
    public CheckRecipeIngredients checkRecipeIngredients(RecipeRepository recipeRepository, IngredientRepository ingredientRepository){
        return new CheckRecipeIngredients(recipeRepository, ingredientRepository);
    }

    @Bean
    public CreateRecipe createRecipe(RecipeRepository recipeRepository){
        return new CreateRecipe(recipeRepository);
    }

    @Bean
    public FindRecipeById findRecipeById(RecipeRepository recipeRepository){
        return new FindRecipeById(recipeRepository);
    }

    @Bean
    public FindRecipeByName findRecipeByName(RecipeRepository recipeRepository){
        return new FindRecipeByName(recipeRepository);
    }

    @Bean
    public GetAllRecipes getAllRecipes(RecipeRepository recipeRepository){
        return new GetAllRecipes(recipeRepository);
    }

    @Bean
    public GetRandomRecipe getRandomRecipe(RecipeRepository recipeRepository){
        return new GetRandomRecipe(recipeRepository);
    }

    @Bean
    public GetRecipeByIngredients getRecipeByIngredients(RecipeRepository recipeRepository, IngredientRepository ingredientRepository, RecipeIngredientRepository recipeIngredientRepository){
        return new GetRecipeByIngredients(recipeRepository, ingredientRepository, recipeIngredientRepository);
    }

    @Bean
    public GetRecipeIngredient getRecipeIngredient(RecipeRepository recipeRepository){
        return new GetRecipeIngredient(recipeRepository);
    }

    @Bean
    public MealToRecipe mealToRecipe(RecipeRepository recipeRepository, RecipeIngredientRepository recipeIngredientRepository, IngredientRepository ingredientRepository){
        return new MealToRecipe(recipeRepository, recipeIngredientRepository, ingredientRepository);
    }

    @Bean
    public RecipeExistsById recipeExistsById(RecipeRepository recipeRepository){
        return new RecipeExistsById(recipeRepository);
    }

    @Bean
    public RecipeExistsByName recipeExistsByName(RecipeRepository recipeRepository){
        return new RecipeExistsByName(recipeRepository);
    }

    @Bean
    public RecipeHasIngredient recipeHasIngredient(RecipeRepository recipeRepository, IngredientRepository ingredientRepository){
        return new RecipeHasIngredient(recipeRepository, ingredientRepository);
    }
    //------------------------------------------------------------//

    //-------------------RECIPEINGREDIENT----------------------//

    @Bean
    public CheckRecipeUsesAllIngredients checkRecipeUsesAllIngredients(RecipeIngredientRepository recipeIngredientRepository){
        return new CheckRecipeUsesAllIngredients(recipeIngredientRepository);
    }

    @Bean
    public CreateRecipeIngredient createRecipeIngredient(RecipeIngredientRepository recipeIngredientRepository){
        return new CreateRecipeIngredient(recipeIngredientRepository);
    }

    @Bean
    public GetQuantityFromRecipeIngredient getQuantityFromRecipeIngredient(RecipeIngredientRepository recipeIngredientRepository){
        return new GetQuantityFromRecipeIngredient(recipeIngredientRepository);
    }

    @Bean
    public GetRecipeIngredientByRecipe getRecipeIngredientByRecipe(RecipeIngredientRepository recipeIngredientRepository){
        return new GetRecipeIngredientByRecipe(recipeIngredientRepository);
    }

    @Bean
    public RecipeIngredientExistsByName recipeIngredientExistsByName(RecipeIngredientRepository recipeIngredientRepository){
        return new RecipeIngredientExistsByName(recipeIngredientRepository);
    }


    //--------------------------------------------------------//


    //------------------STOCK----------------------------------//

    @Bean
    public AddIngredientQuantityToUserStock addIngredientQuantityToUserStock(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository, IngredientRepository ingredientRepository){
        return new AddIngredientQuantityToUserStock(userRepository, stockRepository, stockIngredientRepository, ingredientRepository);
    }

    @Bean
    public CreateStock createStock(StockRepository stockRepository){
        return new CreateStock(stockRepository);
    }

    @Bean
    public DeleteIngredientFromStock deleteIngredientFromStock(UserRepository userRepository,StockRepository stockRepository, IngredientRepository ingredientRepository, StockIngredientRepository stockIngredientRepository){
        return new DeleteIngredientFromStock(userRepository, stockRepository, ingredientRepository, stockIngredientRepository);
    }

    @Bean
    public GetAllIngredientsFromUser getAllIngredientsFromUser(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository){
        return new GetAllIngredientsFromUser(userRepository, stockRepository, stockIngredientRepository);
    }

    @Bean
    public GetAvailableRecipesForUser getAvailableRecipesForUser(UserRepository userRepository, StockIngredientRepository stockIngredientRepository, RecipeRepository recipeRepository){
        return new GetAvailableRecipesForUser(userRepository, stockIngredientRepository, recipeRepository);
    }

    @Bean
    public GetIngredientLimitFromStock getIngredientLimitFromStock(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository, IngredientRepository ingredientRepository){
        return new GetIngredientLimitFromStock(userRepository, stockRepository, stockIngredientRepository, ingredientRepository);
    }

    @Bean
    public GetIngredientQuantityFromStock getIngredientQuantityFromStock(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository, IngredientRepository ingredientRepository){
        return new GetIngredientQuantityFromStock(userRepository, ingredientRepository, stockRepository, stockIngredientRepository);
    }

    @Bean
    public GetIngredientsUnderLimit getIngredientsUnderLimit(UserRepository userRepository, StockRepository stockRepository, StockIngredientRepository stockIngredientRepository){
        return new GetIngredientsUnderLimit(userRepository, stockRepository, stockIngredientRepository);
    }

    @Bean
    public GetUserIngredientQuantity getUserIngredientQuantity(UserRepository userRepository, StockRepository stockRepository){
        return new GetUserIngredientQuantity(userRepository,stockRepository);
    }

    @Bean
    public RecalculateStock recalculateStock(UserRepository userRepository, StockIngredientRepository stockIngredientRepository, RecipeIngredientRepository recipeIngredientRepository, RecipeRepository recipeRepository){
        return new RecalculateStock(userRepository, stockIngredientRepository,recipeIngredientRepository, recipeRepository);
    }

    @Bean
    public UpdateIngredientLimitFromStock updateIngredientLimitFromStock(UserRepository userRepository, StockRepository stockRepository, IngredientRepository ingredientRepository, StockIngredientRepository stockIngredientRepository){
        return new UpdateIngredientLimitFromStock(userRepository, stockRepository, ingredientRepository, stockIngredientRepository);
    }

    @Bean
    public UpdateIngredientQuantityFromStock updateIngredientQuantityFromStock(UserRepository userRepository, StockRepository stockRepository, IngredientRepository ingredientRepository, StockIngredientRepository stockIngredientRepository){
        return new UpdateIngredientQuantityFromStock(userRepository, stockRepository, stockIngredientRepository, ingredientRepository);
    }

    //-------------------------------------------------------------//

    //--------------------------STOCKINGREDIENT-------------------------//

    @Bean
    public AddIngredientQuantityToStock addIngredientQuantityToStock(StockIngredientRepository stockIngredientRepository){
        return new AddIngredientQuantityToStock(stockIngredientRepository);
    }

    @Bean
    public CreateStockIngredient createStockIngredient(StockIngredientRepository stockIngredientRepository){
        return new CreateStockIngredient(stockIngredientRepository);
    }

    @Bean
    public DeleteStockIngredient deleteStockIngredient(StockIngredientRepository stockIngredientRepository){
        return new DeleteStockIngredient(stockIngredientRepository);
    }

    @Bean
    public ExistByIngredientAndStock existByIngredientAndStock(StockIngredientRepository stockIngredientRepository){
        return new ExistByIngredientAndStock(stockIngredientRepository);
    }

    @Bean
    public FindByIngredientAndStock findByIngredientAndStock(StockIngredientRepository stockIngredientRepository){
        return new FindByIngredientAndStock(stockIngredientRepository);
    }

    @Bean
    public GetAllIngredientsFromStock getAllIngredientsFromStock(StockIngredientRepository stockIngredientRepository){
        return new GetAllIngredientsFromStock(stockIngredientRepository);
    }

    @Bean
    public GetLimitFromStockForIngredient getLimitFromStockForIngredient(StockIngredientRepository stockIngredientRepository){
        return new GetLimitFromStockForIngredient(stockIngredientRepository);
    }

    @Bean
    public GetQuantityFromStockForIngredient getQuantityFromStockForIngredient(StockIngredientRepository stockIngredientRepository){
        return new GetQuantityFromStockForIngredient(stockIngredientRepository);
    }

    @Bean
    public UpdateLimitForStockForIngredient updateLimitForStockForIngredient(StockIngredientRepository stockIngredientRepository){
        return new UpdateLimitForStockForIngredient(stockIngredientRepository);
    }

    @Bean
    public UpdateQuantityForStockForIngredient updateQuantityForStockForIngredient(StockIngredientRepository stockIngredientRepository){
        return new UpdateQuantityForStockForIngredient(stockIngredientRepository);
    }

    //------------------------------------------------------------------//

    //---------------------------USER-----------------------------------//

    @Bean
    public CreateUser createUser(UserRepository userRepository){
        return new CreateUser(userRepository);
    }

    @Bean
    public DeleteUser deleteUser(UserRepository userRepository){
        return new DeleteUser(userRepository);
    }

    @Bean
    public FindUserById findUserById(UserRepository userRepository){
        return new FindUserById(userRepository);
    }

    @Bean
    public GetAllUser getAllUser(UserRepository userRepository){ return new GetAllUser(userRepository);
    }
}
