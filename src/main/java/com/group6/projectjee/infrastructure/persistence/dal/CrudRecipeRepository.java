package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.infrastructure.persistence.adapters.IngredientAdapter;
import com.group6.projectjee.infrastructure.persistence.adapters.RecipeAdapter;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.domain.repositories.RecipeRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface CrudRecipeRepository extends CrudRepository<RecipeEntity, Long>, RecipeRepository {
    Boolean existsByName(String name);
    RecipeEntity findByName(String name);

    default Recipe findRecipeByName(String name){
        return RecipeAdapter.parse(findByName(name));
    }

    default Recipe createRecipe(Recipe recipe){
        RecipeEntity recipeEntity = save(RecipeAdapter.parse(recipe));
        return RecipeAdapter.parse(recipeEntity);
    }

    @Override
    default Boolean recipeExistsById(Long id){
        return existsById(id);
    }

    @Override
    default Recipe findRecipeById(Long id){
        Optional<RecipeEntity> recipeEntity = findById(id);
        if(recipeEntity.isPresent()){
            return RecipeAdapter.parse(recipeEntity.get());
        } else {
            return null;
        }
    }

    @Override
    default ArrayList<Recipe> findAllRecipes(){
        ArrayList<Recipe> res = new ArrayList<>();
        findAll().forEach(recipeEntity -> {
            res.add(RecipeAdapter.parse(recipeEntity));
        });
        return res;
    }

    @Override
    default Boolean recipeHasIngredient(Recipe recipe, Ingredient ingredient){
        return recipe.getRecipeIngredients().contains(ingredient);
    }

    @Override
    default ArrayList<Ingredient> getIngredientsFromRecipe(Recipe recipe){
        RecipeEntity recipeEntity = RecipeAdapter.parse(recipe);
        ArrayList<Ingredient> res = new ArrayList<>();
        recipeEntity.getRecipeIngredients().forEach(recipeIngredient -> res.add(IngredientAdapter.parse(recipeIngredient.getIngredientEntity())));
        return res;
    }

    @Override
    default ArrayList<Recipe> findRecipesByIngredient(Ingredient ingredient){
        ArrayList<Recipe> res = new ArrayList<>();
        IngredientEntity ingredientEntity = IngredientAdapter.parse(ingredient);
        ingredientEntity.getRecipeIngredients().forEach(recipeIngredient -> res.add(RecipeAdapter.parse(recipeIngredient.getRecipeEntity())));
        return res;
    }

}
