package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.infrastructure.persistence.adapters.StockAdapter;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.domain.repositories.StockRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrudStockRepository extends CrudRepository<StockEntity, Long>, StockRepository {

    @Override
    default Stock findStockByUser(User user) {
        return StockAdapter.parse(findById(user.getId()).get());
    }

    @Override
    default Stock createStock(Stock stock) {
        return StockAdapter.parse(save(StockAdapter.parse(stock)));
    }
}
