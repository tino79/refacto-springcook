package com.group6.projectjee.infrastructure.persistence.entities;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class Meal {
    @Setter private String strMeal;
    @Getter @Setter private String strArea;
    @Getter @Setter private String strCategory;
    @Getter @Setter private String strIngredient1;
    @Getter @Setter private String strIngredient2;
    @Getter @Setter private String strIngredient3;
    @Getter @Setter private String strIngredient4;
    @Getter @Setter private String strIngredient5;
    @Getter @Setter private String strIngredient6;
    @Getter @Setter private String strInstructions;
    @Getter @Setter private String strMealThumb;
    @Getter @Setter private String strMeasure1;
    @Getter @Setter private String strMeasure2;
    @Getter @Setter private String strMeasure3;
    @Getter @Setter private String strMeasure4;
    @Getter @Setter private String strMeasure5;
    @Getter @Setter private String strMeasure6;

    public String getStrMeal() { return strMeal; }

    public HashMap<String, String> getIngredientsMap() {
        HashMap<String, String> mealIngredients = new HashMap<String, String>();
        mealIngredients.put(strIngredient1, strMeasure1);
        mealIngredients.put(strIngredient2, strMeasure2);
        mealIngredients.put(strIngredient3, strMeasure3);
        mealIngredients.put(strIngredient4, strMeasure4);
        mealIngredients.put(strIngredient5, strMeasure5);
        mealIngredients.put(strIngredient6, strMeasure6);
        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            if(entry.getValue().isEmpty()) {
                entry.setValue("0");
            }
        }
        return mealIngredients;
    }

    public Meal toFrench() {
        Translate translate = TranslateOptions.getDefaultInstance().getService();
        strMeal = translate.translate(strMeal, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strArea = translate.translate(strArea, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strCategory = translate.translate(strCategory, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strIngredient1 = translate.translate(strIngredient1, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strIngredient2 = translate.translate(strIngredient2, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strIngredient3 = translate.translate(strIngredient3, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strIngredient4 = translate.translate(strIngredient4, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strIngredient5 = translate.translate(strIngredient5, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strIngredient6 = translate.translate(strIngredient6, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strInstructions = translate.translate(strInstructions, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strMeasure1 = translate.translate(strMeasure1, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strMeasure2 = translate.translate(strMeasure2, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strMeasure3 = translate.translate(strMeasure3, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strMeasure4 = translate.translate(strMeasure4, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strMeasure5 = translate.translate(strMeasure5, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();
        strMeasure6 = translate.translate(strMeasure6, Translate.TranslateOption.sourceLanguage("en").targetLanguage("fr")).getTranslatedText();

        return this;
    }
}
