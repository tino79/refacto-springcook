package com.group6.projectjee.infrastructure.persistence.entities;

import java.io.Serializable;

public class StockIngredientID implements Serializable {

    private Long stockEntity;
    private Long ingredientEntity;
    private float quantity;
    private float minQuantity;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
