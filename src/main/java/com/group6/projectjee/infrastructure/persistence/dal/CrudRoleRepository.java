package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.enums.ERole;
import com.group6.projectjee.infrastructure.persistence.entities.Role;
import com.group6.projectjee.domain.repositories.RoleRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrudRoleRepository extends CrudRepository<Role, Long>, RoleRepository {

    Role findByName(String name);

    @Override
    default Role findRoleByName(ERole name){
        return findByName(name.toString());
    };
}
