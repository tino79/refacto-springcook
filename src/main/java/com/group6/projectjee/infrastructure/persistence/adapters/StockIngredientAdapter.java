package com.group6.projectjee.infrastructure.persistence.adapters;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;

public class StockIngredientAdapter {

    public static StockIngredient parse(StockIngredientModel stockIngredientModel) {
        StockIngredient stockIngredient = new StockIngredient();
        stockIngredient.setStockEntity(StockAdapter.parse(stockIngredientModel.getStock()));
        stockIngredient.setIngredientEntity(IngredientAdapter.parse(stockIngredientModel.getIngredient()));
        stockIngredient.setQuantity(stockIngredientModel.getQuantity());
        stockIngredient.setMinQuantity(stockIngredientModel.getMinQuantity());
        return stockIngredient;
    }

    public static StockIngredientModel parse(StockIngredient stockIngredient) {
        StockIngredientModel stockIngredientModel = new StockIngredientModel();
        stockIngredientModel.setStock(StockAdapter.parse(stockIngredient.getStockEntity()));
        stockIngredientModel.setIngredient(IngredientAdapter.parse(stockIngredient.getIngredientEntity()));
        stockIngredientModel.setQuantity(stockIngredient.getQuantity());
        stockIngredientModel.setMinQuantity(stockIngredient.getMinQuantity());
        return stockIngredientModel;
    }
}
