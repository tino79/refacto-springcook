package com.group6.projectjee.infrastructure.persistence.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@NamedEntityGraph(
        name = "recipeIngredient-entity-graph",
        attributeNodes = {
                @NamedAttributeNode(value = "ingredientEntity", subgraph = "ingredient-entity-graph")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "ingredient-entity-graph",
                        attributeNodes = {
                                @NamedAttributeNode("id"),
                                @NamedAttributeNode("name"),
                                @NamedAttributeNode("baseUnit"),
                        }
                )
        }
)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@IdClass(RecipeIngredientID.class)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"recipe_id", "ingredient_id"}))
public class RecipeIngredient implements Serializable {

    // jointure recipe - ingredient
    @Id
    @ManyToOne
    @JoinColumn(name = "recipe_id", referencedColumnName = "id")
    @JsonBackReference
    private RecipeEntity recipeEntity;

    @Id
    @ManyToOne
    @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    @JsonBackReference
    private IngredientEntity ingredientEntity;

    @Column(nullable = false)
    private float quantity;

}

