package com.group6.projectjee.infrastructure.persistence.entities.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;

import java.io.IOException;
import java.util.List;

public class RecipeIngredientSerializer extends JsonSerializer<List<RecipeIngredient>> {

    @Override
    public void serialize(List<RecipeIngredient> recipeIngredients, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for(RecipeIngredient recipeIngredient : recipeIngredients){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("Ingredient Name", recipeIngredient.getIngredientEntity().getName());
            jsonGenerator.writeStringField("Recipe Name", recipeIngredient.getRecipeEntity().getName());
            jsonGenerator.writeStringField("Quantity", String.valueOf(recipeIngredient.getQuantity()));
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
