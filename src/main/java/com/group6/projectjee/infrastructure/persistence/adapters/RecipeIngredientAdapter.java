package com.group6.projectjee.infrastructure.persistence.adapters;

import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;

public class RecipeIngredientAdapter {

    public static RecipeIngredientModel parse(RecipeIngredient recipeIngredient){
        RecipeIngredientModel res = new RecipeIngredientModel(
                RecipeAdapter.parse(recipeIngredient.getRecipeEntity()),
                IngredientAdapter.parse(recipeIngredient.getIngredientEntity()),
                recipeIngredient.getQuantity()
        );
        return res;
    }

    public static RecipeIngredient parse(RecipeIngredientModel recipeIngredientModel){
        RecipeIngredient res = new RecipeIngredient();
        res.setRecipeEntity(RecipeAdapter.parse(recipeIngredientModel.getRecipe()));
        res.setIngredientEntity(IngredientAdapter.parse(recipeIngredientModel.getIngredient()));
        res.setQuantity(recipeIngredientModel.getQuantity());
        return res;
    }
}
