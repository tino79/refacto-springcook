package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.infrastructure.persistence.adapters.IngredientAdapter;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.domain.repositories.IngredientRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface CrudIngredientRepository extends CrudRepository<IngredientEntity, Long>, IngredientRepository {

    Boolean existsByName(String name);
    IngredientEntity findByName(String name);

    @Override
    default Boolean ingredientExistsByName(String name){
        return existsByName(name);
    }

    @Override
    default Boolean ingredientExistsById(Long id){
        return existsById(id);
    }

    @Override
    default Ingredient findIngredientByName(String name){
        return IngredientAdapter.parse(findByName(name));
    }

    @Override
    default Iterable<Ingredient> findAllIngredients(){
        ArrayList<Ingredient> res = new ArrayList<>();
        findAll().forEach(ingredientEntity -> res.add(IngredientAdapter.parse(ingredientEntity)));
        return res;
    }

    @Override
    default Ingredient createIngredient(Ingredient ingredient){
        IngredientEntity ingredientEntity = save(IngredientAdapter.parse(ingredient));
        return IngredientAdapter.parse(ingredientEntity);
    }

    @Override
    default Ingredient findIngredientById(Long id){
        Optional<IngredientEntity> ingredientEntity = findById(id);
        if(ingredientEntity.isPresent()){
            return IngredientAdapter.parse(ingredientEntity.get());
        } else {
            return null;
        }
    }

}
