package com.group6.projectjee.infrastructure.persistence.entities.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;

import java.io.IOException;
import java.util.List;

public class StockIngredientsSerializer extends JsonSerializer<List<StockIngredient>> {
    @Override
    public void serialize(List<StockIngredient> stockIngredientList, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for(StockIngredient stockIngredient : stockIngredientList){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("Stock", stockIngredient.getStockEntity().getName());
            jsonGenerator.writeStringField("Ingredient", stockIngredient.getIngredientEntity().getName());
            jsonGenerator.writeStringField("Min quantity", String.valueOf(stockIngredient.getMinQuantity()));
            jsonGenerator.writeStringField("quantity", String.valueOf(stockIngredient.getQuantity()));
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
