package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.domain.models.business.RecipeIngredientModel;
import com.group6.projectjee.infrastructure.persistence.adapters.IngredientAdapter;
import com.group6.projectjee.infrastructure.persistence.adapters.RecipeAdapter;
import com.group6.projectjee.infrastructure.persistence.adapters.RecipeIngredientAdapter;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import com.group6.projectjee.domain.repositories.RecipeIngredientRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface CrudRecipeIngredientRepository extends CrudRepository<RecipeIngredient, Long>, RecipeIngredientRepository {

    RecipeIngredient getByIngredientEntityAndRecipeEntity(IngredientEntity ingredientEntity, RecipeEntity recipeEntity);
    Boolean existsByIngredientEntityNameAndAndRecipeEntityName(String recipeName, String ingredientName);
    RecipeIngredient findByRecipeEntityName(String recipeName);
    RecipeIngredient findByIngredientEntityEntityName(String ingredientName);
    Float getQuantityByIngredientEntityAndRecipeEntity(Ingredient ingredient, Recipe recipe);

    default boolean existsByRecipeEntityNameAndIngredientEntityName(String recipe_name, String ingredient_name){
        return existsByIngredientEntityNameAndAndRecipeEntityName(ingredient_name, recipe_name);
    }

    @Override
    default RecipeIngredientModel getByRecipeEntityAndIngredientEntity(Recipe recipe, Ingredient ingredient){
        return RecipeIngredientAdapter.parse(
                getByIngredientEntityAndRecipeEntity(
                    IngredientAdapter.parse(ingredient),
                    RecipeAdapter.parse(recipe)
                )
        );
    }

    @Override
    default RecipeIngredientModel createRecipeIngredientModel(RecipeIngredientModel recipeIngredientModel){
        RecipeIngredient recipeIngredient = save(RecipeIngredientAdapter.parse(recipeIngredientModel));
        return RecipeIngredientAdapter.parse(recipeIngredient);
    }

    @Override
    default Boolean usesAllIngredients(String recipe_name, String[] ingredients){
        RecipeEntity recipeEntity = findByRecipeEntityName(recipe_name).getRecipeEntity();
        for(String ingredient : ingredients){
            IngredientEntity ingredientEntity = findByIngredientEntityEntityName(ingredient).getIngredientEntity();
            if(!recipeEntity.getRecipeIngredients().contains(ingredientEntity)){
                return false;
            }
        }
        return true;
    }

    @Override
    default Float getIngredientQuantity(Recipe recipe, Ingredient ingredient) {
        return this.getQuantityByIngredientEntityAndRecipeEntity(ingredient, recipe);
    }

}
