package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.infrastructure.persistence.adapters.UserAdapter;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.domain.repositories.UserRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface CrudUserRepository extends CrudRepository<UserEntity, Long>, UserRepository {

    UserEntity findByUsername(String username);
    Optional<UserEntity> findById(Long id);
    Boolean existByUsername(String username);
    Boolean existByUserEmail (String email);

    @Override
    default User findUserByUsername(String username){
        return UserAdapter.parse(findByUsername(username));
    };

    @Override
    default User findUserById(Long id){
        return UserAdapter.parse(findById(id).get());
    };

    @Override
    default Boolean userExistsByUsername(String username){
        return existByUsername(username);
    };

    @Override
    default Boolean userExistsByEmail(String email){
        return this.existByUserEmail(email);
    };

    @Override
    default User createUser(User user){
        return UserAdapter.parse(save(UserAdapter.parse(user)));
    }

    @Override
    default void deleteUser(User user){
        delete(UserAdapter.parse(user));
    }

    @Override
    default Boolean userExistsById(Long id){
        return existsById(id);
    }

    @Override
    default ArrayList<User> findAllUsers(){
        ArrayList<User> res = new ArrayList<>();
        findAll().forEach(userEntity -> res.add(UserAdapter.parse(userEntity)));
        return res;
    }
}
