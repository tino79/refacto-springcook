package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.domain.models.business.StockIngredientModel;
import com.group6.projectjee.infrastructure.persistence.adapters.IngredientAdapter;
import com.group6.projectjee.infrastructure.persistence.adapters.StockAdapter;
import com.group6.projectjee.infrastructure.persistence.adapters.StockIngredientAdapter;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;
import com.group6.projectjee.domain.repositories.StockIngredientRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public interface CrudStockIngredientRepository extends CrudRepository<StockIngredient, Long>, StockIngredientRepository {
    @Override
    default boolean stockIngredientExistsByStockEntity(Stock stock) {
        return existsById(stock.getId());
    }

    @Override
    default void deleteStockIngredient(StockIngredientModel stockIngredientModel) {
        delete(StockIngredientAdapter.parse(stockIngredientModel));
    }

    @Override
    default List<StockIngredientModel> getAllStockIngredient() {
        Iterable<StockIngredient> stockIngredient = findAll();
        return StreamSupport.stream(stockIngredient.spliterator(), false).map(StockIngredientAdapter::parse).collect(Collectors.toList());
    }

    @Override
    default StockIngredientModel createStockIngredient(StockIngredientModel stockIngredientModel) {
        StockIngredient stockIngredient = save(StockIngredientAdapter.parse(stockIngredientModel));
        return StockIngredientAdapter.parse(stockIngredient);
    }

    @Override
    default StockIngredientModel getStockIngredientByStockEntity(Stock stock) {
        StockEntity stockEntity = StockAdapter.parse(stock);
        return StockIngredientAdapter.parse(findById(stockEntity.getId()).get());
    }

    @Override
    default boolean stockIngredientExistsByStockEntityAndIngredientEntity(Stock stock, Ingredient ingredient) {
        return existsById(ingredient.getId());
    }

    @Override
    default StockIngredientModel getStockIngredientByIngredientEntityAndStockEntity(Ingredient ingredient, Stock stock) {
        return StockIngredientAdapter.parse(findById(ingredient.getId()).get());
    }

    Float findQuantityByIngredientEntityNameAndStockEntityName(String ingredient, String stock);

    @Override
    default Float getStockIngredientQuantity(Ingredient ingredient, Stock stock) {
        return findQuantityByIngredientEntityNameAndStockEntityName(ingredient.getName(), stock.getName());
    }

    float findMinQuantityByIngredientEntityNameAndStockEntityName(String ingredient, String stock);

    @Override
    default float getStockIngredientMinLimit(Ingredient ingredient, Stock stock) {
        return findMinQuantityByIngredientEntityNameAndStockEntityName(ingredient.getName(), stock.getName());
    }

    List<StockIngredient> findAllByStockEntityName(String name);

    @Override
    default List<Ingredient> getAllIngredientEntityByStock(Stock stock) {
        ArrayList<IngredientEntity> results = new ArrayList<>();
        for (StockIngredient stockEntry :
                findAllByStockEntityName(stock.getName())) {
            results.add(stockEntry.getIngredientEntity());
        }
        return results.stream().map(IngredientAdapter::parse).collect(Collectors.toList());
    }

    @Modifying
    @Transactional
    @Query(value = "UPDATE StockIngredient set quantity=:newQuantity where stock_id=:stockEntity and ingredient_id=:ingredientEntity")
    void updateStockIngredientQuantity(@Param("ingredientEntity") Ingredient ingredient, @Param("stockEntity") Stock stock, @Param("newQuantity") Float newQuantity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE StockIngredient set minQuantity=:newLimit where stock_id=:stockEntity and ingredient_id=:ingredientEntity")
    void updateLimit(@Param("ingredientEntity") Ingredient ingredient, @Param("stockEntity") Stock stock, @Param("newLimit") Float newLimit);
}
