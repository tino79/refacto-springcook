package com.group6.projectjee.infrastructure.persistence.adapters;

import com.group6.projectjee.domain.models.business.Recipe;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;

import java.util.stream.Collectors;

public class RecipeAdapter {

    public static RecipeEntity parse(Recipe recipe){
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setId(recipe.getId());
        recipeEntity.setName(recipe.getName());
        recipeEntity.setCreatedAt(recipe.getCreatedAt());
        recipeEntity.setUpdatedAt(recipe.getUpdatedAt());
        recipeEntity.setRecipeIngredients(recipe.getRecipeIngredients().stream().map(RecipeIngredientAdapter::parse).collect(Collectors.toList()));
        return recipeEntity;
    }

    public static Recipe parse(RecipeEntity recipeEntity){
        Recipe recipe = new Recipe();
        recipe.setId(recipeEntity.getId());
        recipe.setName(recipeEntity.getName());
        recipe.setCreatedAt(recipeEntity.getCreatedAt());
        recipe.setUpdatedAt(recipeEntity.getUpdatedAt());
        recipe.setRecipeIngredients(recipeEntity.getRecipeIngredients().stream().map(RecipeIngredientAdapter::parse).collect(Collectors.toList()));
        return recipe;
    }
}
