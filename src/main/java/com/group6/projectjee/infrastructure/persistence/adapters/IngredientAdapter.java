package com.group6.projectjee.infrastructure.persistence.adapters;

import com.group6.projectjee.domain.models.business.Ingredient;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;

import java.util.stream.Collectors;

public class IngredientAdapter {

    public static IngredientEntity parse(Ingredient ingredient){
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setId(ingredient.getId());
        ingredientEntity.setName(ingredient.getName());
        ingredientEntity.setBaseUnit(ingredient.getBaseUnit());
        ingredientEntity.setRecipeIngredients(
                ingredient
                        .getRecipeIngredients()
                        .stream()
                        .map(RecipeIngredientAdapter::parse)
                        .collect(Collectors.toList())
        );
        ingredientEntity.setStockIngredients(ingredient.getStockIngredients());
        return ingredientEntity;
    }

    public static Ingredient parse(IngredientEntity ingredientEntity){
        Ingredient ingredient = new Ingredient();
        ingredient.setId(ingredientEntity.getId());
        ingredient.setName(ingredientEntity.getName());
        ingredient.setBaseUnit(ingredientEntity.getBaseUnit());
        ingredient.setRecipeIngredients(
                ingredientEntity
                        .getRecipeIngredients()
                .stream()
                .map(RecipeIngredientAdapter::parse)
                .collect(Collectors.toList())
        );
        ingredient.setStockIngredients(ingredientEntity.getStockIngredients());
        return ingredient;
    }
}
