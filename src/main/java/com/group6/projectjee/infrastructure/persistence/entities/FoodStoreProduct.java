package com.group6.projectjee.infrastructure.persistence.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FoodStoreProduct {
    private String code;
    private String brand;
    private String foodstore;
    private String name;
    private Float price;
}
