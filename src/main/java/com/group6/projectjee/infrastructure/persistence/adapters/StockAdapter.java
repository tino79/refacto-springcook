package com.group6.projectjee.infrastructure.persistence.adapters;

import com.group6.projectjee.domain.models.business.Stock;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;

public class StockAdapter {

    public static StockEntity parse(Stock stock){
        StockEntity stockEntity = new StockEntity();
        stockEntity.setId(stock.getId());
        stockEntity.setName(stock.getName());
        //stockEntity.setUserEntity(UserAdapter.parse(stock.getUser()));
        stockEntity.setCreatedAt(stock.getCreatedAt());
        stockEntity.setUpdatedAt(stock.getUpdatedAt());
        stockEntity.setStockIngredients(stock.getStockIngredients());
        return stockEntity;
    }

    public static Stock parse(StockEntity stockEntity){
        Stock stock = new Stock();
        stock.setId(stockEntity.getId());
        stock.setName(stockEntity.getName());
        stock.setUser(UserAdapter.parse(stockEntity.getUserEntity()));
        stock.setCreatedAt(stockEntity.getCreatedAt());
        stock.setUpdatedAt(stockEntity.getUpdatedAt());
        stock.setStockIngredients(stockEntity.getStockIngredients());
        return stock;
    }

}
