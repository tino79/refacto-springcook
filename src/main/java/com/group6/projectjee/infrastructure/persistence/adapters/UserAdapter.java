package com.group6.projectjee.infrastructure.persistence.adapters;

import com.group6.projectjee.domain.models.users.User;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;

public class UserAdapter {

    public static UserEntity parse(User user){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId());
        userEntity.setUsername(user.getUsername());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setRoles(user.getRoles());
        userEntity.setCreatedAt(user.getCreatedAt());
        userEntity.setUpdatedAt(user.getUpdatedAt());
        userEntity.setPassword(user.getPassword());
        userEntity.setEmail(user.getEmail());
        return userEntity;
    }

    public static User parse(UserEntity userEntity){
        User user = new User();
        user.setId(userEntity.getId());
        user.setUsername(userEntity.getUsername());
        user.setFirstName(userEntity.getFirstName());
        user.setLastName(userEntity.getLastName());
        user.setRoles(userEntity.getRoles());
        user.setCreatedAt(userEntity.getCreatedAt());
        user.setUpdatedAt(userEntity.getUpdatedAt());
        user.setPassword(userEntity.getPassword());
        user.setEmail(userEntity.getEmail());
        return user;
    }

}
