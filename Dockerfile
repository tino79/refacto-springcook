FROM maven:latest as maven-builder
WORKDIR /app
#WORKDIR /workspace
#COPY . /workspace/
COPY . /app/
#RUN mvn package

#Start with base JAVA image

#copy ..

RUN mvn clean install

FROM openjdk:8
WORKDIR /app


#COPY --from=maven-builder /workspace/target /app
COPY --from=maven-builder /app/target/projectjee-0.0.1-SNAPSHOT.jar /app/projectjee.jar

EXPOSE 8081

#Application jar file
#ARG JAR_FILE=target/projectjee-0.0.1-SNAPSHOT.jar

#ADD Application's jar file to the container
#ADD ${JAR_FILE} projectjee.jar

#RUN
#CMD ["sh", "-c","java -jar *.jar"]
CMD ["java", "-jar", "projectjee.jar"]
